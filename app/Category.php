<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\library\ListingReturnData;

class Category extends MyAppModel
{

    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'image', 'parent_id'];

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function scopeSubcategories($query, int $category_id)
    {
        return $query->where('parent_id', '=', $category_id);
    }


    protected static function boot() {
        parent::boot();
        static::deleting(function($category) {
            foreach ( $category->subcategories($category->id)->get() as $subCategory ) {
                foreach ( $subCategory->tasks()->get() as $task ) {
                    $task->delete();
                }
                foreach ( $subCategory->tasks()->get() as $task ) {
                    $task->delete();
                }
                $subCategory->delete();
            }
        });

    }



    /* return data array by keys id/name based on filters/ordering... */
    public static function getCategoriesSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $categoriesList = Category::getCategoriesList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($categoriesList as $nextCategory) {
            if ($key_return) {
                $resArray[] = array('key' => $nextCategory->id, 'label' => $nextCategory->id.'='.$nextCategory->name);
            } else{
                $resArray[ $nextCategory->id ]= $nextCategory->name;
            }
        }
        return $resArray;
    }
    
    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getCategoriesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'c.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $category_table_name= with(new Category)->getTableName();
        $quoteModel= Category::from(  \DB::raw(DB::getTablePrefix().$category_table_name.' as c' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'c.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Category::myStrLower('name', false, false) . ' like ' . Category::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Category::myStrLower('name', false, false) . ' like ' . Category::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . Category::myStrLower('description', false, false) . ' like ' . Category::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Category::myStrLower('description', false, false) . ' like ' . Category::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "c.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "c.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Category)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Category)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $categorysList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $categorysList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $categorysList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $categorysList as $next_key=>$nextCategory ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextCategory['created_at_label']= with(new Category)->getFormattedDateTime($nextCategory->created_at);
            }
            if (!empty($filtersArray['short_description'])) {
                $nextCategory['description']= with(new Category)->concatStr($nextCategory->description,50);
            }
        }
        return $categorysList;

    } // public static function getCategoriesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $category = Category::find($id);
            if ( $category == null ) return false;
            if (!empty($additiveParams['fill_labels'])) {
                $category['created_at_label']= with(new Category)->getFormattedDateTime($category->created_at);
            }
        }
        return $category;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($category_id) : array
    {
        $additional_name_validation_rule= 'check_category_unique_by_name:'.( !empty($category_id)?$category_id:'');
        $validationRulesArray = [
            'name'          => 'required|max:50|'.$additional_name_validation_rule,
            'description'   => 'nullable|max:255',
            'parent_id'     => 'nullable|exists:'.( with(new Category)->getTableName() ).',id',
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for categories.name field */
    public static function getSimilarCategoryByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Category::whereRaw( Category::myStrLower('name', false, false) .' = '. Category::myStrLower( Category::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}

