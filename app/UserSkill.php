<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\library\ListingReturnData;
use App\Events\UserSkillUpdatingEvent;

class UserSkill extends MyAppModel
{

    protected $table = 'user_skills';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['skill', 'user_id', 'rating' ];

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserSkillsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'us.skill'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_skill_table_name= with(new UserSkill)->getTableName();
        $quoteModel= UserSkill::from(  \DB::raw(DB::getTablePrefix().$user_skill_table_name.' as us' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'us.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['skill'])) {
            $quoteModel->whereRaw( UserSkill::myStrLower('skill', false, false) . ' like ' . UserSkill::myStrLower( $filtersArray['skill'], true,true ));
        }


        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('us.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['only_positive_rating'])) {
            $quoteModel->whereRaw( DB::raw('us.rating > 0')  );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("us.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("us.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_user_name'])  ) { // need to join in select sql username and user_status field of author of uc
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as user_name, u_c.status as user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('us.user_id') );
        } // if ( !empty($filtersArray[show_user_name])  ) { // need to join in select sql username and user_status field of author of uc

        if ( !empty($filtersArray['show_manager_name'])  ) { // need to join in select sql username and user_status field of author of uc
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_m.name as manager_name, u_m.status as  manager_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('us.manager_id') );
        } // if ( !empty($filtersArray[show_manager_name])  ) { // need to join in select sql username and user_status field of author of uc

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserSkill)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserSkill)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userSkillsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userSkillsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userSkillsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userSkillsList as $next_key=>$nextUserSkill ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserSkill['created_at_label']= with(new UserSkill)->getFormattedDateTime($nextUserSkill->created_at);
                $nextUserSkill['updated_at_label']= with(new UserSkill)->getFormattedDateTime($nextUserSkill->updated_at);
                $nextUserSkill['status_label']= UserSkill::getUserSkillStatusLabel($nextUserSkill->status);
            }
        }
        return $userSkillsList;

    } // public static function getUserSkillsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Skill
            $additive_fields_for_select= "";
            $fields_for_select= 'us.*';
            $user_skill_table_name= with(new UserSkill)->getTableName();
            $quoteModel= UserSkill::from(  \DB::raw(DB::getTablePrefix().$user_skill_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('us.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as user_name, u_c.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('us.user_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $userSkillRows = $quoteModel->get();
            if (!empty($userSkillRows[0]) and get_class($userSkillRows[0]) == 'App\UserSkill' ) {
                $userSkill= $userSkillRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Skill


        if ( !$is_row_retrieved ) {
            $userSkill = UserSkill::find($id);
        }

        if (empty($userSkill)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userSkill['created_at_label']= with(new UserSkill)->getFormattedDateTime($userSkill->created_at);
            $userSkill['updated_at_label']= with(new UserSkill)->getFormattedDateTime($userSkill->updated_at);
            $userSkill['status_label']= UserSkill::getUserSkillStatusLabel($userSkill->status);
        }
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $userSkill= with(new UserSkill)->substituteNullValues($userSkill, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $userSkill;     // 'set_null_fields_space'=>['task_id']
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $user_skill_id, bool $check_user_skill_participant_selected= false ) : array
    {
        $additional_name_validation_rule= 'check_user_skill_unique_by_name:'.( !empty($user_skill_id)?$user_skill_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:100|'.$additional_name_validation_rule,
            'user_id'      => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'rating'       => 'required|numeric|min:0|min:10',
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for user_skills.name field */
    public static function getSimilarUserSkillByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = UserSkill::whereRaw( UserSkill::myStrLower('name', false, false) .' = '. UserSkill::myStrLower( UserSkill::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getDistinctUserSkillsList( string $skill= '' )
    {
        $skillsList = UserSkill:: whereRaw( UserSkill::myStrLower('skill', false, false) . ' like ' . UserSkill::myStrLower( $skill, true,true ))->
          select('skill')->distinct()->orderBy(\DB::raw('skill'), 'asc' )->get();
        return $skillsList;
    }


}

