<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Exceptions\NotLoggedUserException;
use App\Http\Traits\funcsTrait;
use function PHPSTORM_META\type;

class ChatEvent  implements ShouldBroadcast
{
    use funcsTrait;

    public $user_chat_message_id;
    public $user_id;
    public $user_chat_id;
    public $is_top;
    public $author_name;
    public $text;

    public $user;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(int $user_chat_message_id, int $user_id, int $user_chat_id, int $is_top, string $text, User $user)
    {
//        $this->debToFile( gettype($user),'  app/Events/ChatEvent.php  gettype($user)::');
//        $this->debToFile($user,'  app/Events/ChatEvent.php  $user:');
        throw_if($user == null, new NotLoggedUserException('To send chat messages user must be logged !'));
//        $this->debToFile($user_chat_message_id,'  app/Events/ChatEvent.php  user_chat_message_id:');

        $this->user_chat_message_id = $user_chat_message_id;
        $this->user_id = $user_id;
        $this->author_name = $user->first_name.' '.$user->last_name;
//        $this->debToFile( $this->author_name,'  app/Events/ChatEvent.php  $this->author_name::');

        $this->user_chat_id = $user_chat_id;
        $this->is_top = $is_top;
        $this->text = $text;

        $this->user = $user;
        $this->dontBroadcastToCurrentUser();
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('nsn_tasks_chat');
    }
}
