<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\library\ListingReturnData;

class Debugging extends MyAppModel
{

    protected $table = 'debugging';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [ 'user_id', 'info', 'type' ];
    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getDebuggingsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'dg.priority';
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $debugging_table_name= with(new Debugging)->getTableName();
        $quoteModel= Debugging::from(  \DB::raw(DB::getTablePrefix().$debugging_table_name.' as dg' ));
        $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));

        $additive_fields_for_select= "";
        $fields_for_select= 'dg.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['text'])) {
            $quoteModel->whereRaw( Debugging::myStrLower('text', false, false) . ' like ' . Debugging::myStrLower( $filtersArray['text'], true,true ));
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('dg.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['type'])) {
            $quoteModel->where( DB::raw('dg.type'), '=', $filtersArray['type'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("dg.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("dg.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Debugging)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Debugging)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $debuggingsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $debuggingsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $debuggingsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $debuggingsList as $next_key=>$nextDebugging ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextDebugging['created_at_label']= with(new Debugging)->getFormattedDateTime($nextDebugging->created_at);
            }
        }
        return $debuggingsList;

    } // public static function getDebuggingsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Todo
            $additive_fields_for_select= "";
            $fields_for_select= 'dg.*';
            $debugging_table_name= with(new Debugging)->getTableName();
            $quoteModel= Debugging::from(  \DB::raw(DB::getTablePrefix().$debugging_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('dg.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as user_name, u_c.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('dg.user_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $debuggingRows = $quoteModel->get();
            if (!empty($debuggingRows[0]) and get_class($debuggingRows[0]) == 'App\Debugging' ) {
                $debugging= $debuggingRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Todo


        if ( !$is_row_retrieved ) {
            $debugging = Debugging::find($id);
        }

        if (empty($debugging)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $debugging['created_at_label']= with(new Debugging)->getFormattedDateTime($debugging->created_at);
        }

        return $debugging;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $debugging_id ) : array
    {
        $validationRulesArray = [
            'text'            => 'required|max:255',
            'user_id'         => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'info'            => 'required',
            'type'            => 'nullable',
            'completed'       => 'required|in:'.with( new Debugging)->getValueLabelKeys( Debugging::getDebuggingCompletedValueArray(false) ),
        ];

        return $validationRulesArray;
    }

}