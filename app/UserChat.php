<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\UserChatMessage;
use App\UserChatParticipant;
use App\UserChatLastVisited;
use App\UserChatMessageDocument;
use App\UserChatNewMessage;
use App\library\ListingReturnData;
use App\Events\UserChatUpdatingEvent;
use App\Rules\CheckUserChatParticipantSelected;

class UserChat extends MyAppModel
{

    protected $table = 'user_chats';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userChatStatusValueArray = Array('A' => 'Active', 'C' => 'Closed');

    protected $dispatchesEvents = [
        'updating' => UserChatUpdatingEvent::class,
    ];

    public function userChatParticipants()
    {
        return $this->hasMany('App\UserChatParticipant');
    }

    public function userChatMessageDocuments()
    {
        return $this->hasMany('App\UserChatMessageDocument');
    }

    public function userChatLastVisited()
    {
        return $this->hasMany('App\UserChatLastVisited');
    }

    public function userChatMessages()
    {
        return $this->hasMany('App\UserChatMessage');
    }

//    public function userChatNewMessages()
//    {
//        return $this->hasMany('App\UserChatNewMessage');
//    }

    public function task()
    {
        return $this->hasOne('App\Task');
    }
    /*class Post extends Model
{
    /
     * Get the comments for the blog post.
     
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}*/

    protected static function boot() {
        parent::boot();

        self::deleting(function($userChat) {
            $userChat->userChatParticipants()->delete();
            $userChat->userChatMessages()->delete();
            $userChat->userChatMessageDocuments()->delete();
            $userChat->userChatLastVisited()->delete();
        });
    }

    /* return array of key value/label pairs based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatStatusValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userChatStatusValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatStatusLabel(string $status) : string
    {
        if (!empty(self::$userChatStatusValueArray[$status])) {
            return self::$userChatStatusValueArray[$status];
        }
        return '';
    }


    protected $fillable = ['name', 'description', 'creator_id', 'task_id', 'status', 'manager_id'];

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'uc.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_chat_table_name= with(new UserChat)->getTableName();
        $quoteModel= UserChat::from(  \DB::raw(DB::getTablePrefix().$user_chat_table_name.' as uc' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'uc.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( UserChat::myStrLower('name', false, false) . ' like ' . UserChat::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.UserChat::myStrLower('name', false, false) . ' like ' . UserChat::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . UserChat::myStrLower('description', false, false) . ' like ' . UserChat::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . UserChat::myStrLower('description', false, false) . ' like ' . UserChat::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        $is_participant_user_id_joined= false;
        if (!empty($filtersArray['participant_user_id'])) {
            $user_chat_participant_table_name= DB::getTablePrefix().with(new UserChatParticipant)->getTableName();
            $quoteModel->join( \DB::raw($user_chat_participant_table_name . ' as ucp '), \DB::raw('ucp.user_chat_id'), '=', \DB::raw('uc.id') );
            $quoteModel->where( \DB::raw('ucp.user_id'), '=', $filtersArray['participant_user_id'] );
            $is_participant_user_id_joined= true;
        }


        if (!empty($filtersArray['creator_id'])) {
            $quoteModel->where( DB::raw('uc.creator_id'), '=', $filtersArray['creator_id'] );
        }

        if (!empty($filtersArray['task_id'])) {
            $quoteModel->where( DB::raw('uc.task_id'), '=', $filtersArray['task_id'] );
        }

        if (!empty($filtersArray['status'])) {
            $quoteModel->where( DB::raw('uc.status'), '=', $filtersArray['status'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("uc.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("uc.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $user_chat_message_table_name= DB::getTablePrefix().( with(new UserChatMessage)->getTableName() );
        if ( !empty($filtersArray['show_user_chat_messages_count_by_user_id']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $user_chat_message_table_name . ' as ucm_1 where ucm_1.user_chat_id = ' . 'uc.id and ucm_1.user_id = '.$filtersArray['show_user_chat_messages_count_by_user_id'].' ) as user_chat_messages_count_by_user_id';
        }

        if ( !empty($filtersArray['show_user_chat_messages_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $user_chat_message_table_name . ' as ucm where ucm.user_chat_id = ' . 'uc.id ) as user_chat_messages_count';
        }

        // show_user_chat_participants_count
        $user_chat_participant_table_name= DB::getTablePrefix().( with(new UserChatParticipant)->getTableName() );
        if ( !empty($filtersArray['show_user_chat_participants_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $user_chat_participant_table_name . ' as ucm where ucm.user_chat_id = ' . 'uc.id ) as user_chat_participants_count';
        }

        if ( !empty($filtersArray['show_creator_name'])  ) { // need to join in select sql username and user_status field of author of uc
//            echo '<pre>-1::</pre>';
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as creator_name, u_c.status as creator_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('uc.creator_id') );
        } // if ( !empty($filtersArray[show_creator_name])  ) { // need to join in select sql username and user_status field of author of uc

        if ( !empty($filtersArray['show_manager_name'])  ) { // need to join in select sql username and user_status field of author of uc
//            echo '<pre>-2::</pre>';
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_m.name as manager_name, u_m.status as  manager_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('uc.manager_id') );
        } // if ( !empty($filtersArray[show_manager_name])  ) { // need to join in select sql username and user_status field of author of uc

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChat)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChat)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userChatsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userChatsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userChatsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userChatsList as $next_key=>$nextUserChat ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserChat['created_at_label']= with(new UserChat)->getFormattedDateTime($nextUserChat->created_at);
                $nextUserChat['updated_at_label']= with(new UserChat)->getFormattedDateTime($nextUserChat->updated_at);
                $nextUserChat['status_label']= UserChat::getUserChatStatusLabel($nextUserChat->status);
            }
            if (!empty($filtersArray['short_description'])) {
                $nextUserChat['description']= with(new UserChat)->concatStr($nextUserChat->description,50);
            }
        }
        return $userChatsList;

    } // public static function getUserChatsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_creator_name']) ) { // need to join in select sql username and user_status field of author of user Chat
            $additive_fields_for_select= "";
            $fields_for_select= 'uc.*';
            $user_chat_table_name= with(new UserChat)->getTableName();
            $quoteModel= UserChat::from(  \DB::raw(DB::getTablePrefix().$user_chat_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('uc.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as creator_name, u_c.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('uc.creator_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $userChatRows = $quoteModel->get();
            if (!empty($userChatRows[0]) and get_class($userChatRows[0]) == 'App\UserChat' ) {
                $is_row_retrieved= true;
                $userChat= $userChatRows[0];
            }
        } // if (!empty($additiveParams['show_creator_name']) ) { // need to join in select sql username and user_status field of author of user Chat

        if (!empty($additiveParams['show_manager_name']) ) { // need to join in select sql username and user_status field of author of user Chat
            $additive_fields_for_select= "";
            $fields_for_select= 'uc.*';
            $user_chat_table_name= with(new UserChat)->getTableName();
            $quoteModel= UserChat::from(  \DB::raw(DB::getTablePrefix().$user_chat_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('uc.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_m.name as manager_name, u_m.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('uc.manager_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $userChatRows = $quoteModel->get();
            if (!empty($userChatRows[0]) and get_class($userChatRows[0]) == 'App\UserChat' ) {
                $is_row_retrieved= true;
                $userChat= $userChatRows[0];
            }
        } // if (!empty($additiveParams['show_manager_name']) ) { // need to join in select sql username and user_status field of author of user Chat

        if ( !$is_row_retrieved ) {
            $userChat = UserChat::find($id);
        }

        if (empty($userChat)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userChat['created_at_label']= with(new UserChat)->getFormattedDateTime($userChat->created_at);
            $userChat['updated_at_label']= with(new UserChat)->getFormattedDateTime($userChat->updated_at);
            $userChat['status_label']= UserChat::getUserChatStatusLabel($userChat->status);
        }
//        echo '<pre>====$additiveParams::'.print_r($additiveParams,true).'</pre>';
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $userChat= with(new UserChat)->substituteNullValues($userChat, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $userChat;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $user_chat_id, bool $check_user_chat_participant_selected= false ) : array
    {
//        echo '<pre>++ getValidationRulesArray::'.print_r($_POST,true).'</pre>';
        $additional_name_validation_rule= 'check_user_chat_unique_by_name:'.( !empty($user_chat_id)?$user_chat_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:255|'.$additional_name_validation_rule,
            'description'  => 'required',
            'creator_id'   => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'task_id'      => 'nullable|exists:'.( with(new Task)->getTableName() ).',id',
            'status'       => 'required|in:'.with( new UserChat)->getValueLabelKeys( UserChat::getUserChatStatusValueArray(false) ),
            'userChatParticipantsArray'=> [ 'required',new CheckUserChatParticipantSelected() ]
        ];

        return $validationRulesArray;
    }

    /* check if provided name is unique for user_chats.name field */
    public static function getSimilarUserChatByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = UserChat::whereRaw( UserChat::myStrLower('name', false, false) .' = '. UserChat::myStrLower( UserChat::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}

