<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Traits\funcsTrait;

class CheckEventUserSelected implements Rule
{

    use funcsTrait;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function passes($attribute, $eventUsersArray)
    {
        if (empty($eventUsersArray) or !is_array($eventUsersArray)) return false;

        $selected_users_count= 0;
        foreach( $eventUsersArray as $next_key=>$nextEventUser ) {
            $selected_users_count++;
        }
        if ( $selected_users_count< 1 ) return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Select at least 1 user.';
    }
}
