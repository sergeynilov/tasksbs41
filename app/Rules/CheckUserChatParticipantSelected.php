<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Traits\funcsTrait;

class CheckUserChatParticipantSelected implements Rule
{

    use funcsTrait;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function passes($attribute, $userChatParticipantsArray)
    {
        if (empty($userChatParticipantsArray) or !is_array($userChatParticipantsArray)) return false;

        $selected_users_count= 0;
        $selected_managers_count= 0;
        foreach( $userChatParticipantsArray as $next_key=>$nextUserChatParticipant ) {
            if ( !empty($nextUserChatParticipant['participant_status']) ) {
                $selected_users_count++;
                if ( $nextUserChatParticipant['participant_status'] == 'M' ) {
                    $selected_managers_count++;
                }
            }
        }
        if ( $selected_users_count<= 1 or $selected_managers_count != 1 ) return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Select at least 2 users and only 1 of them can be manager.';
    }
}
