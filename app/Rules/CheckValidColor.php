<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckValidColor implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function passes($attribute, $color_value)
    {
        $valid_color_format= config('app.valid_color_format','~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~');

        $ret= preg_match($valid_color_format, $color_value, $matches, PREG_OFFSET_CAPTURE);
        return $ret;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The color must have format #123 or #d245f3 ';
    }
}
