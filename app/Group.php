<?php

namespace App;

use DB;

class Group extends MyAppModel
{

    protected $table = 'groups';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];



    /* check if provided name is unique for groups.name field */
    public static function getSimilarGroupByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Group::whereRaw( Group::myStrLower('name', false, false) .' = '. Group::myStrLower( Group::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


}