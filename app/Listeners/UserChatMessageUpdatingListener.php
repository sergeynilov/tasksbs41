<?php

namespace App\Listeners;

use App\Events\UserChatMessageUpdatingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;


class UserChatMessageUpdatingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserChatMessageUpdatingEvent $event)
    {
        $event->userChatMessage->updated_at= Carbon::now();
    }

}
