<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\MyAppModel;
use App\User;
use App\Task;
use App\UserProfileDocument;
use App\DocumentCategory;
use App\library\ListingReturnData;

class EventUser extends MyAppModel
{

    protected $fillable = [ 'user_id', 'event_id'];
/* CREATE TABLE `tsk_events_users` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`event_id` INT(10) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, */
    protected $table = 'events_users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function task(){
        return $this->belongsTo('App\Task', 'task_id','id');
    }

    public function event(){
        return $this->belongsTo('App\Event', 'event_id','id');
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getEventUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'eu.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $event_user_table_name= with(new EventUser)->getTableName();
        $quoteModel= EventUser::from(  \DB::raw(DB::getTablePrefix().$event_user_table_name.' as eu' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'eu.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( \DB::raw('eu.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['task_id'])) {
            $quoteModel->where( \DB::raw('e.task_id'), '=', $filtersArray['task_id'] );
        }
        if (!empty($filtersArray['event_id'])) {
            $quoteModel->where( \DB::raw('eu.event_id'), '=', $filtersArray['event_id'] );
        }


        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( \DB::raw("eu.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( \DB::raw("eu.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $is_event_joined= false;
        if ( !empty($filtersArray['show_task_name']) and !empty($filtersArray['show_event_name']) ) { // need to join in select sql task name and task_status field and event name and event_access field

            $is_event_joined= true;
            $events_table_name= DB::getTablePrefix() . ( with(new Event)->getTableName() );
            $tasks_table_name= DB::getTablePrefix() . ( with(new Task)->getTableName() );
            $additive_fields_for_select .= ', t.name as task_name, t.status as task_status, e.name as event_name, e.access as event_access, e.at_time as event_at_time, e.duration as event_duration ' ;

            $quoteModel->leftJoin( \DB::raw($events_table_name . ' as e '), \DB::raw('e.id'), '=', \DB::raw('eu.event_id') );
            $quoteModel->leftJoin( \DB::raw($tasks_table_name . ' as t '), \DB::raw('t.id'), '=', \DB::raw('e.task_id') );
        } // if ( !empty($filtersArray['show_task_name'])  ) { // need to join in select sql task name and task_status field

        //             $filtersArray= [ 'user_id'=> $user_id, 'at_time_start'=> $begin_of_day , 'at_time_end'=> $end_of_day ];
        if ( !empty($filtersArray['at_time_start']) and !empty($filtersArray['at_time_end']) ) {
            if ( !$is_event_joined ) {
                $events_table_name= DB::getTablePrefix() . ( with(new Event)->getTableName() );
                $is_event_joined = true;
                $additive_fields_for_select .= ', e.name as event_name, e.access as event_access, e.at_time as event_at_time, e.duration as event_duration ' ;
                $quoteModel->leftJoin( \DB::raw($events_table_name . ' as e '), \DB::raw('e.id'), '=', \DB::raw('eu.event_id') );
            }
            $quoteModel->whereRaw( \DB::raw("e.at_time >= '").$filtersArray['at_time_start'] . "' and " . \DB::raw("e.at_time <'").$filtersArray['at_time_end'] . "' " );
        }



        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */


        $items_per_page= with(new EventUser)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new EventUser)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $eventUsersList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $eventUsersList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $eventUsersList = $quoteModel->get();
            $data_retrieved= true;
        }

        foreach( $eventUsersList as $next_key=>$nextEventUser ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['set_former_at_time'])) {
                $nextEventUser['event_at_time_is_past']= with(new Event)->getDateTimesCompare( $nextEventUser->event_at_time, time() );
            }
            if (!empty($filtersArray['fill_labels'])) {
                $nextEventUser['event_at_time_label']= with(new Event)->getFormattedDateTime($nextEventUser->event_at_time);
                $nextEventUser['created_at_label']= with(new Event)->getFormattedDateTime($nextEventUser->created_at);

                if ( !empty($nextEventUser->task_status) ) {
                    $nextEventUser['task_status_label'] = with(new Task)->getTaskStatusLabel($nextEventUser->task_status);
                }
                if ( !empty($nextEventUser->event_access) ) {
                    $nextEventUser['event_access_label'] = with(new Event)->getEventAccessLabel($nextEventUser->event_access);
                }
            }
        }

        return $eventUsersList;

        /* .name as event_name, e.access as event_access  */

    } // public static function getEventUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $eventUser = EventUser::find($id);
        }

        if (empty($eventUser)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $eventUser['created_at_label']= with(new EventUser)->getFormattedDateTime($eventUser->created_at);
        }
        return $eventUser;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($user_chat_id) : array
    {
        $validationRulesArray = [
            'user_id'        => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'task_id'        => 'required|exists:'.( with(new Task)->getTableName() ).',id',
        ];
        return $validationRulesArray;
    }



}

