<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\library\ListingReturnData;
use App\Events\UserWeatherLocationUpdatingEvent;

class UserWeatherLocation extends MyAppModel
{

    protected $table = 'user_weather_locations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userWeatherLocationValueArray = Array('C'=>'By city name', 'O' => 'By city id', 'G' => 'By geographic coordinates', 'Z' => 'By ZIP code');

    protected $fillable = ['location', 'user_id', 'location_type', 'ordering' ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    /* return array of key value/label pairs based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserWeatherLocationValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userWeatherLocationValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userWeatherLocationValueArray - db enum key values/labels implementation */
    public static function getUserWeatherLocationValueArrayLabel(string $location_type) : string
    {
        if (!empty(self::$userWeatherLocationValueArray[$location_type])) {
            return self::$userWeatherLocationValueArray[$location_type];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserWeatherLocationsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'uwl.location'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_weather_location_table_name= with(new UserWeatherLocation)->getTableName();
        $quoteModel= UserWeatherLocation::from(  \DB::raw(DB::getTablePrefix().$user_weather_location_table_name.' as uwl' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'uwl.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['location'])) {
            $quoteModel->whereRaw( UserWeatherLocation::myStrLower('location', false, false) . ' like ' . UserWeatherLocation::myStrLower( $filtersArray['location'], true,true ));
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('uwl.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['location_type'])) {
            $quoteModel->where( DB::raw('uwl.location_type'), '=', $filtersArray['location_type'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("uwl.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("uwl.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserWeatherLocation)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserWeatherLocation)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userWeatherLocationsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userWeatherLocationsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userWeatherLocationsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userWeatherLocationsList as $next_key=>$nextUserWeatherLocation ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserWeatherLocation['created_at_label']= with(new UserWeatherLocation)->getFormattedDateTime($nextUserWeatherLocation->created_at);
                $nextUserWeatherLocation['location_type_label']= UserWeatherLocation::getUserWeatherLocationValueArrayLabel($nextUserWeatherLocation->location_type);
            }
        }
        return $userWeatherLocationsList;

    } // public static function getUserWeatherLocationsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Weather Location
            $additive_fields_for_select= "";
            $fields_for_select= 'uwl.*';
            $user_weather_location_table_name= with(new UserWeatherLocation)->getTableName();
            $quoteModel= UserWeatherLocation::from(  \DB::raw(DB::getTablePrefix().$user_weather_location_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('uwl.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as user_name, u_c.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('uwl.user_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $userWeatherLocationRows = $quoteModel->get();
            if (!empty($userWeatherLocationRows[0]) and get_class($userWeatherLocationRows[0]) == 'App\UserWeatherLocation' ) {
                $userWeatherLocation= $userWeatherLocationRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Weather Location


        if ( !$is_row_retrieved ) {
            $userWeatherLocation = UserWeatherLocation::find($id);
        }

        if (empty($userWeatherLocation)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userWeatherLocation['created_at_label']= with(new UserWeatherLocation)->getFormattedDateTime($userWeatherLocation->created_at);
            $userWeatherLocation['location_type_label']= UserWeatherLocation::getUserWeatherLocationValueArrayLabel($userWeatherLocation->location_type);
        }
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $userWeatherLocation= with(new UserWeatherLocation)->substituteNullValues($userWeatherLocation, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $userWeatherLocation;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $user_weather_location_id ) : array
    {
        $validationRulesArray = [
            'location'        => 'required|max:255',
            'user_id'         => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'location_type'   => 'required|in:'.with( new UserWeatherLocation)->getValueLabelKeys( UserWeatherLocation::getUserWeatherLocationValueArray(false) ),
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for user_weather_locations.location field */
    public static function getSimilarUserWeatherLocationByLocation( string $location, int $user_id, int $id= null, bool $return_count = false )
    {
        $quoteModel = UserWeatherLocation::whereRaw( UserWeatherLocation::myStrLower('location', false, false) .' = '. UserWeatherLocation::myStrLower( UserWeatherLocation::mysqlEscape($location), true,false) );
        $quoteModel = $quoteModel->where( 'user_id', '=' , $user_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getMaxOrdering() : int
    {
        $max_ordering = UserWeatherLocation::max('ordering');
        return empty($max_ordering)? 1 : $max_ordering+1;
    }

}