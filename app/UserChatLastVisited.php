<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\library\ListingReturnData;

class UserChatLastVisited extends MyAppModel
{

    protected $fillable = [ 'user_id', 'user_chat_id' ];

    protected $table = 'user_chats_last_visited';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function userChat(){
        return $this->belongsTo('App\UserChat', 'user_chat_id','id');
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatsLastVisitedList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param=
    0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'uclv.visited_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_chats_last_visited_table_name= with(new UserChatLastVisited)->getTableName();
        $quoteModel= UserChatLastVisited::from(  \DB::raw(DB::getTablePrefix().$user_chats_last_visited_table_name.' as uclv' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'uclv.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( 'user_id', '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['user_chat_id'])) {
            $quoteModel->where( 'user_chat_id', '=', $filtersArray['user_chat_id'] );
        }


        if (!empty($filtersArray['visited_at_from'])) {
            $quoteModel->whereRaw( "uclv.visited_at >='".$filtersArray['visited_at_from'] . "'" );
        }
        if (!empty($filtersArray['visited_at_till'])) {
            $quoteModel->whereRaw( "uclv.visited_at <='".$filtersArray['visited_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u.name as username, u.first_name, u.last_name, u.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('uclv.user_id') );
        } // if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChatLastVisited)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChatLastVisited)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $userChatLastVisitedsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userChatLastVisitedsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userChatLastVisitedsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userChatLastVisitedsList as $next_key=>$nextUserChatLastVisited ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserChatLastVisited['visited_at_label']= with(new UserChatLastVisited)->getFormattedDateTime($nextUserChatLastVisited->visited_at);
            }
        }
        return $userChatLastVisitedsList;

    } // public static function getUserChatLastVisitedsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {


    public static function updateVisitedAt( int $user_id, int $user_chat_id )
    {
        $similarUserChatLastVisited= UserChatLastVisited::getSimilarUserChatLastVisitedByUserIdAndUserChatId( $user_id, $user_chat_id);
        if ( $similarUserChatLastVisited == null ) {
            $similarUserChatLastVisited= new UserChatLastVisited();
            $similarUserChatLastVisited->user_id= $user_id;
            $similarUserChatLastVisited->user_chat_id= $user_chat_id;
        }
        $similarUserChatLastVisited->visited_at= now();
        $similarUserChatLastVisited->save();
    }


    public static function getValidationRulesArray($user_chat_id) : array
    {
        $validationRulesArray = [
            'user_id'        => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'user_chat_id'   => 'required|exists:'.( with(new UserChat)->getTableName() ).',id',
        ];
        return $validationRulesArray;
    }

    /* check if provided $user_chat_id/$user_chat_id is unique for user_chats_last_visited.name field */
    public static function getSimilarUserChatLastVisitedByUserIdAndUserChatId( int $user_id, int $user_chat_id, bool $return_count = false )
    {
        $quoteModel = UserChatLastVisited::where( 'user_id', $user_id );
        $quoteModel = $quoteModel->where( 'user_chat_id', $user_chat_id );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


}

