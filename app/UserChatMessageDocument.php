<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image as Image;

use App\MyAppModel;
use App\User;
use App\Config;
use App\DocumentCategory;
use App\UserChatMessage;
use App\library\ListingReturnData;

class UserChatMessageDocument extends MyAppModel
{

    protected $table = 'user_chat_message_documents';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $userChatMessageDocumentImagePropsArray = [];
    protected static $img_filename_max_length= 50;
    protected static $img_preview_width= 128;
    protected static $img_preview_height= 96;

    protected $fillable = [ 'user_chat_message_id', 'filename', 'user_id', 'document_category_id' ];
    
    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatMessageDocumentsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'ucmd.filename'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $document_categories_table_name= with(new DocumentCategory)->getTableName();
        $user_chat_message_document_table_name= with(new UserChatMessageDocument)->getTableName();
        $quoteModel= UserChatMessageDocument::from(  \DB::raw(DB::getTablePrefix().$user_chat_message_document_table_name.' as ucmd' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ucmd.*';

        if (!empty($filtersArray['filename'])) {
            $quoteModel->whereRaw( UserChatMessageDocument::myStrLower('filename', false, false) . ' like ' . UserChatMessageDocument::myStrLower( $filtersArray['filename'], true,true ));
        }

        if (!empty($filtersArray['user_chat_message_id'])) {
            $quoteModel->where( DB::raw('ucmd.user_chat_message_id'), '=', $filtersArray['user_chat_message_id'] );
        }

        if (!empty($filtersArray['user_chat_id'])) {
            $quoteModel->where(DB::raw('ucmd.user_chat_id'), '=', $filtersArray['user_chat_id']);
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('ucmd.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['document_category_id'])) {
            $quoteModel->where( DB::raw('ucmd.document_category_id'), '=', $filtersArray['document_category_id'] );
        }

        if (!empty($filtersArray['document_category_type'])) {
            if (is_array($filtersArray['document_category_type'])) {
                $quoteModel->join(DB::raw(DB::getTablePrefix() . $document_categories_table_name . ' as dc '), \DB::raw('dc.id'), '=', \DB::raw('ucmd.document_category_id'));
                $quoteModel->whereIn(DB::raw('dc.type'), $filtersArray['document_category_type']);
            } else {
                $quoteModel->join(DB::raw(DB::getTablePrefix() . $document_categories_table_name . ' as dc '), \DB::raw('dc.id'), '=', \DB::raw('ucmd.document_category_id'));
                $quoteModel->where(DB::raw('dc.type'), '=', $filtersArray['document_category_type']);
            }
        }

        if (!empty($filtersArray['public_access'])) {
            $quoteModel->where(DB::raw('ucmd.public_access'), '=', $filtersArray['public_access']);
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "ucmd.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "ucmd.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChatMessageDocument)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChatMessageDocument)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $documentCategoriesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }
        $base_url= with(new UserChatMessageDocument)->getBaseUrl();
        $imagesExtensionsArray= \Config::get('app.images_extensions');
        foreach( $documentCategoriesList as $next_key=>$nextUserChatMessageDocument ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserChatMessageDocument['created_at_label']= with(new UserChatMessageDocument)->getFormattedDateTime($nextUserChatMessageDocument->created_at);
//                $nextUserChatMessageDocument['type_label']= UserChatMessageDocument::getUserChatMessageDocumentTypeLabel($nextUserChatMessageDocument->type);
            }
            if (!empty($filtersArray['short_info'])) {
                $nextUserChatMessageDocument['info']= with(new UserChatMessageDocument)->concatStr($nextUserChatMessageDocument->info,50);
            }


            if (!empty($filtersArray['show_image']) or !empty($filtersArray['show_image_info']) ) {
                if ( ! empty($filtersArray['show_image_info'])) {
                    $next_user_chat_message_document_filename= UserChatMessageDocument::getUserChatMessageDocumentPath($nextUserChatMessageDocument->id,
                        $nextUserChatMessageDocument->filename, false);
                    $next_user_chat_message_document_url= $base_url.Storage::disk('local')->url($next_user_chat_message_document_filename );
                    $file_exists = Storage::disk('local')->exists('public/'.$next_user_chat_message_document_filename);
                    if ( $file_exists and in_array($nextUserChatMessageDocument->extension, $imagesExtensionsArray) ) { // that is image - get all props
                        $image             = $nextUserChatMessageDocument->filename;
                        $image_path        = public_path('storage/'.$next_user_chat_message_document_filename);
                        $image_url         = $next_user_chat_message_document_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $previewSizeArray= with(new UserChatMessageDocument)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height );
//                            echo '<pre>::'.print_r($previewSizeArray,true).'</pre>';
                        if ( !empty($previewSizeArray['width']) ) {
                            $imagePropsArray['preview_width']= $previewSizeArray['width'];
                            $imagePropsArray['preview_height']= $previewSizeArray['height'];
                        }
                        $nextUserChatMessageDocumentImgPropsArray= UserChatMessageDocument::getImageProps( $image_path, $imagePropsArray );
//                            echo '<pre>$nextUserChatMessageDocumentImgPropsArray::'.print_r($nextUserChatMessageDocumentImgPropsArray,true).'</pre>';
                        if ( !empty($nextUserChatMessageDocumentImgPropsArray) and is_array($nextUserChatMessageDocumentImgPropsArray) ) {
                            foreach( $nextUserChatMessageDocumentImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $documentCategoriesList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    } // if ( $file_exists and in_array($nextUserChatMessageDocument->extension, $imagesExtensionsArray) ) { // that is image - get all props
                    if ( $file_exists and !in_array($nextUserChatMessageDocument->extension, $imagesExtensionsArray) ) { // that is NOT image - get only some props
                        $image             = $nextUserChatMessageDocument->filename;
                        $image_path        = public_path('storage/'.$next_user_chat_message_document_filename);
                        $image_url         = $next_user_chat_message_document_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $nextUserChatMessageDocumentImgPropsArray= UserChatMessageDocument::getImageProps( $image_path, $imagePropsArray );
//                            echo '<pre>$nextUserChatMessageDocumentImgPropsArray::'.print_r($nextUserChatMessageDocumentImgPropsArray,true).'</pre>';
                        if ( !empty($nextUserChatMessageDocumentImgPropsArray) and is_array($nextUserChatMessageDocumentImgPropsArray) ) {
                            foreach( $nextUserChatMessageDocumentImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $documentCategoriesList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    }
                } // if ( ! empty($filtersArray['show_image_info'])) {
            }

        }
        return $documentCategoriesList;

    } // public static function getUserChatMessageDocumentsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $userChatMessageDocument = UserChatMessageDocument::find($id);
        }

        if (empty($userChatMessageDocument)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userChatMessageDocument['created_at_label']= with(new UserChatMessageDocument)->getFormattedDateTime($userChatMessageDocument->created_at);
        }
        return $userChatMessageDocument;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function getValidationRulesArray($user_chat_id= null) : array
    {
        $additional_filename_validation_rule= 'check_user_chat_message_unique_by_filename:'.( !empty($user_chat_id)?$user_chat_id:'');
        $additional_filename_valid_extension_validation_rule= 'check_filename_valid_extension:'.( !empty($user_chat_id)?$user_chat_id:'');
        $validationRulesArray = [
            'user_chat_id'           => 'required|exists:'.( with(new UserChat)->getTableName() ).',id',
            'user_id'                => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'filename'               => 'required|max:255|' . $additional_filename_validation_rule,
            'extension'              => 'required|max:10|' . $additional_filename_valid_extension_validation_rule,
            'document_category_id'   => 'required|exists:'.( with(new DocumentCategory)->getTableName() ).',id',
            'info'                   => 'nullable',
        ];
        return $validationRulesArray;
    }


    public static function getSimilarUserChatMessageDocumentByFilename( string $filename, int $user_chat_id, int $id= null, $return_count= false )
    {
        $quoteModel = UserChatMessageDocument::whereRaw( UserChatMessageDocument::myStrLower('filename', false, false) .' = '. UserChatMessageDocument::myStrLower(UserChatMessageDocument::mysqlEscape($filename), true,false) );
        $quoteModel = $quoteModel->where( 'user_chat_id', '=' , $user_chat_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getUserChatMessageDocumentDir(int $user_chat_message_document_id, bool $short_path= false) : string
    {
        $public_path= '';  //with (new UserChatMessageDocument)->getPublicPath();
        $UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR= \Config::get('app.UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR');
        return $public_path . '/' . $UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR . '-user_chat_message_document-' . $user_chat_message_document_id.'/';
    }

    public static function getUserChatMessageDocumentPath(int $user_chat_message_document_id, $filename, bool $set_public_subdirectory= false, bool $short_path= false) : string
    {
        $public_path= '';//public_path().'/';
        if ( empty($filename) ) return '';
        $UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR');
//        echo '<pre>$public_path::'.print_r($public_path,true).'</pre>';
        return ( !$short_path ? $public_path /*.  '/' */  : '' ) . $UPLOADS_USER_CHAT_MESSAGE_DOCUMENTS_DIR . '-user_chat_message_document-' . $user_chat_message_document_id . '/' . $filename;
    }

    public function getUserChatMessageDocumentImagePropsAttribute($only_existing_files= false) : array
    {
        return $this->userChatMessageDocumentImagePropsArray;
    }

    public function setUserChatMessageDocumentImagePropsAttribute(array $userChatMessageDocumentImagePropsArray)
    {
        $this->userChatMessageDocumentImagePropsArray = $userChatMessageDocumentImagePropsArray;
    }



}
