<?php

namespace App\Http\Controllers;

use App\Settings;
use Auth;
use DB;
//use File;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;
//use Symfony\Component\Process\Process;
//use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

//use Faker\Factory as Faker;
use Faker;
use Database\Factories\EventFactory;
use Database\Factories\EventUserFactory;
use App\User;
use App\UserProfiles;
use App\UsersGroups;
use App\Task;
use App\DocumentCategory;
use App\UserProfileDocument;
use App\UserChat;
use App\Event;
use App\UserWeatherLocation;
use App\UserTodo;
use App\EventUser;
use App\TaskAssignedToUser;
use App\Http\Requests\UserWeatherLocationRequest;
use App\Http\Requests\UserTodoRequest;

class DashboardController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->addVariablesToJS();
        $site_name  = Settings::getValue('site_name', '');
        return view('admin.dashboard.index',['site_name'=> $site_name]);
    }

    function formatSql($sql, $is_break_line = true, $is_include_html = true)
    {
        $break_line = '';
        $space_char = '  ';
        if ($is_break_line) {
            if ( $is_include_html ) {
                $break_line = '<br>';
            }  else {
                $break_line = PHP_EOL;
            }
        }
        $bold_start= '';
        $bold_end= '';
        if ( $is_include_html ) {
            $bold_start= '<B>';
            $bold_end= '</B>';
        }
        $sql = ' ' . $sql . ' ';
        $left_cond= '~\b(?<![%\'])';
        $right_cond= '(?![%\'])\b~i';
        $sql = preg_replace( $left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char .$bold_start . "INSERT INTO" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql );
        $sql = preg_replace( $left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "and" . $right_cond,  $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "or" . $right_cond,  $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "as" . $right_cond,  $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "exists" . $right_cond,  $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);
        return $sql;
    } // function formatSql($sql, $is_break_line = true, $is_include_html = true)

    public function test()
    {
        $this->addVariablesToJS();
        return view('admin.test.index');

    }


//$filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'name'=>''];
//$page= (int)$this->getParameter( 'page', 1 );
//$order_by= $this->getParameter( 'order_by', 'name' );
//$order_direction= $this->getParameter( 'order_direction', 'asc' );
//
//try {
//$rows_count= Task::getTasksList( ListingReturnData::ROWS_COUNT, $filtersArray );


    public function dashboard_profile_data()
    {
        $loggedUser      = Auth::user();
        $is_admin= $this->checkUserGroupAccess("Admin",[], $loggedUser);
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        $order_by= 'task_priority_task_date_end';
        $forAdminAssigningTasksAssignedToUserList= [];
        $forAdminProcessingTasksAssignedToUserList= [];
        $forAdminCheckingTasksAssignedToUserList= [];

        $tasksAssignedToUserAsTeamLeaderAssigningList= [];
        $tasksAssignedToUserAsTeamLeaderProcessingList= [];
        $tasksAssignedToUserAsTeamLeaderCheckingList= [];

        $activeUserChatsList= [];
        $yourActiveUserChatsList= [];
        $yourClosedUserChatsList= [];
        $yourEventUsersArray= [];
        try {
            $assigningTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );
            $processingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );
            $checkingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );


            $tasksAssignedToUserAsTeamLeaderList = TaskAssignedToUser::getTaskAssignedToUsersAsTeamLeaderList( $loggedUser->id );   // Get listing of tasks where logged
            // user is admin
            $tasksInResultsArray= [];
            foreach( $tasksAssignedToUserAsTeamLeaderList as $next_key=>$nextTasksAssignedToUserAsTeamLeader ) {
//                if ( $nextTasksAssignedToUserAsTeamLeader['task_id'] != 1 ) continue;
                if ( in_array($nextTasksAssignedToUserAsTeamLeader['task_id'], $tasksInResultsArray) ) continue;
                $tasksInResultsArray[]= $nextTasksAssignedToUserAsTeamLeader['task_id'];

                $tempAssigningList= TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                foreach( $tempAssigningList as $nextTempAssigning ) {
                    $tasksAssignedToUserAsTeamLeaderAssigningList[]= $nextTempAssigning->toArray();
                }

                $tempProcessingList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                foreach( $tempProcessingList as $nextTempProcessing ) {
                    $tasksAssignedToUserAsTeamLeaderProcessingList[]= $nextTempProcessing->toArray();
                }

                $tempCheckingList= TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                foreach( $tempCheckingList as $nextTempChecking ) {
                    $tasksAssignedToUserAsTeamLeaderCheckingList[]= $nextTempChecking->toArray();
                }
            }

            if ( $is_admin ) { // admin is logged
                $forAdminAssigningTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $forAdminProcessingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $forAdminCheckingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );


                $activeUserChatsList = [];//TODOUserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'A', 'fill_labels' => 1, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );
                $yourActiveUserChatsList = [];//TODOUserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'A', 'fill_labels' => 1, 'show_user_chat_messages_count_by_user_id'=> $loggedUser->id, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );
                $yourClosedUserChatsList = [];//TODOUserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'C', 'fill_labels' => 1, 'show_user_chat_messages_count_by_user_id'=> $loggedUser->id, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );

                $yourEventsList = [];//TODOEvent::getEventsList(ListingReturnData::LISTING, [ 'only_future' => '', 'user_id' => $loggedUser->id, 'show_task_name'=> 1], 'e.created_at', 'asc' );
                $yourEventUsersArray= [];
                foreach( $yourEventsList as $next_key=>$nextEventUser ) {
                    $end_time= $this->addMinutesToFormattedDateTime($nextEventUser->at_time, $nextEventUser->duration);
                    $time_format= 'H:i A';
                    if (   $this->getFormattedDateTime($nextEventUser->at_time, 'Y-m-d' ) != $this->getFormattedDateTime($end_time, 'Y-m-d' )   ) {
                        $time_format= 'Y-m-d H:i:s';  // NOT the same day
                    }

                    $event_at_time_is_past= $nextEventUser->getDateTimesCompare($nextEventUser->at_time);
                    $yourEventUsersArray[]= [
                        'id'=> $nextEventUser->id,
                        'title'=> $nextEventUser->name,
                        'description'=> $nextEventUser->description,
                        'start'=> $nextEventUser->at_time,
                        'start_time_label'=> $this->getFormattedDateTime($nextEventUser->at_time, $time_format ),
                        'end'=> $end_time,
                        'end_time_label'=> $this->getFormattedDateTime($end_time, $time_format ),
                        'duration'=> $nextEventUser->duration,
                        'access'=> $nextEventUser->access,
                        'event_at_time_is_past'=> $event_at_time_is_past,
                        'event_class_name' => [ $event_at_time_is_past] , 'YOUR_DATA' => ['val'=>'Let it be.'] ];
                }

//                echo '<pre>$yourEventUsersArray::'.print_r($yourEventUsersArray,true).'</pre>';
//                die("-1 XXZ=====");
//                $rows_count= UserChat::getUserChatsList( ListingReturnData::ROWS_COUNT, $filtersArray );

            }
        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigningTasksAssignedToUserList'   => null,
                'processingTasksAssignedToUserList'  => null,
                'checkingTasksAssignedToUserList'    => null,

                'tasksAssignedToUserAsTeamLeaderAssigningList'   => null,
                'tasksAssignedToUserAsTeamLeaderProcessingList'  => null,
                'tasksAssignedToUserAsTeamLeaderCheckingList'    => null,

                'forAdminAssigningTasksAssignedToUserList' => null,
                'forAdminProcessingTasksAssignedToUserList' => null,
                'forAdminCheckingTasksAssignedToUserList'   => null,
                'activeUserChatsList'                => null,
                'yourActiveUserChatsList'            => null,
                'yourClosedUserChatsList'            => null,
                'yourEventUsersArray'            => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }


        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigningTasksAssignedToUserList'       => $assigningTasksAssignedToUserList,
            'processingTasksAssignedToUserList'      => $processingTasksAssignedToUserList,
            'checkingTasksAssignedToUserList'        => $checkingTasksAssignedToUserList,

            'tasksAssignedToUserAsTeamLeaderAssigningList'   => $tasksAssignedToUserAsTeamLeaderAssigningList,
            'tasksAssignedToUserAsTeamLeaderProcessingList'  => $tasksAssignedToUserAsTeamLeaderProcessingList,
            'tasksAssignedToUserAsTeamLeaderCheckingList'    => $tasksAssignedToUserAsTeamLeaderCheckingList,

            'forAdminAssigningTasksAssignedToUserList'    => $forAdminAssigningTasksAssignedToUserList,
            'forAdminProcessingTasksAssignedToUserList'   => $forAdminProcessingTasksAssignedToUserList,
            'forAdminCheckingTasksAssignedToUserList'     => $forAdminCheckingTasksAssignedToUserList,
            'activeUserChatsList'                         => $activeUserChatsList,
            'yourActiveUserChatsList'                     => $yourActiveUserChatsList,
            'yourClosedUserChatsList'                     => $yourClosedUserChatsList,
            'yourEventUsersArray'                         => $yourEventUsersArray,

        ], HTTP_RESPONSE_OK );

    } // public function dashboard_profile_data()


    public function refresh_user_profile_quick_info()
    {
        $loggedUser      = Auth::user();
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        try {
            $assigning_tasks_count = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id, 'status' => 'A'] );


            $user_weather_locations_count = UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id] );
            $user_todos_count = UserTodo::getUserTodosList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id] );

            $future_events_count = Event::getEventsList(ListingReturnData::ROWS_COUNT, [ 'only_future' => 1, 'user_id' => $loggedUser->id] );
            $today_events_count = Event::getEventsList(ListingReturnData::ROWS_COUNT, [ 'only_today' => 1, 'user_id' => $loggedUser->id] );
            $tomorrow_events_count = Event::getEventsList(ListingReturnData::ROWS_COUNT, [ 'only_tomorrow' => 1, 'user_id' => $loggedUser->id] );


        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigning_tasks_count'              => null,
                'future_events_count'                => null,
                'tomorrow_events_count'                => null,
                'today_events_count'                 => null,
                'user_weather_locations_count'       => null,
                'user_todos_count'       => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigning_tasks_count'                  => $assigning_tasks_count,
            'future_events_count'                    => $future_events_count,
            'tomorrow_events_count'                  => $tomorrow_events_count,
            'today_events_count'                     => $today_events_count,
            'user_weather_locations_count'           => $user_weather_locations_count,
            'user_todos_count'                       => $user_todos_count,
        ], HTTP_RESPONSE_OK );

    } // public function refresh_user_profile_quick_info()


    public function get_assigning_tasks_details()
    {
        $loggedUser      = Auth::user();
        try {
            $assigningTasksListing = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, [ 'fill_labels'=> 1, 'user_id' => $loggedUser->id, 'status' => 'A',
                'show_username'=> 1, 'show_tasks_info'=> 1, 'show_user_task_type_name'=> 1 ] );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigningTasksListing'              => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigningTasksListing'                  => $assigningTasksListing,
        ], HTTP_RESPONSE_OK );

    } // public function get_assigning_tasks_details()



    
    public function get_user_view_data($id)
    {
        try {
//            $previewUser = User::find($id, []);
            $previewUser = User::getRowById($id, ['fill_labels'=> 1]);
            $users_groups_text= UsersGroups::getUsersGroupsTextLabel($previewUser->id,true);

            $profileThumbnailImageDocumentCategory= DocumentCategory::getSimilarDocumentCategoryByAlias('profile_thumbnail_image');
            $user_profile_thumbnail_image_url= '';
//            echo '<pre>$profileThumbnailImageDocumentCategory::'.print_r($profileThumbnailImageDocumentCategory,true).'</pre>';
            if ( $profileThumbnailImageDocumentCategory!= null ) {
                $userProfileDocument= UserProfileDocument::getUserProfileDocumentsList( ListingReturnData::LISTING,
                    ['document_category_id'=>$profileThumbnailImageDocumentCategory->id, 'user_id'=>$id] );
//                echo '<pre>$userProfileDocument[0]::'.print_r($userProfileDocument[0],true).'</pre>';
                if ( !empty($userProfileDocument[0]) ) {
                    $base_url= self::getBaseUrl();
                    $next_user_profile_document_filename= UserProfileDocument::getUserProfileDocumentPath($userProfileDocument[0]->user_id,$userProfileDocument[0]->filename,  false);
                    $next_user_profile_document_url= $base_url.Storage::disk('local')->url($next_user_profile_document_filename );
//                    echo '<pre>$::'.print_r($next_user_profile_document_filename,true).'</pre>';
                    $file_exists = Storage::disk('local')->exists('public/'.$next_user_profile_document_filename);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                    if ( $file_exists ) {
                        $user_profile_thumbnail_image_url= $next_user_profile_document_url;
                    }
                }
            }
//            echo '<pre>$user_profile_thumbnail_image_url::'.print_r($user_profile_thumbnail_image_url,true).'</pre>';
//            die("-1 XXZ----");


        } catch (Exception $e) {
            return response()->json( [ 'error_code' => 1, 'message' => $e->getMessage(), 'user' => null, 'users_groups_text'  => null, 'user_profile_thumbnail_image_url'=> null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( [
            'error_code'=> 0,
            'message'=> '',
            'user'  => $previewUser,
            'users_groups_text'  => $users_groups_text,
            'user_profile_thumbnail_image_url'=> $user_profile_thumbnail_image_url

        ], HTTP_RESPONSE_OK );
    } //public function get_user_view_data()

    
    public function get_settings_value()
    {
        try {
            $name        = $this->getParameter('name');
            $default_value        = $this->getParameter('default_value');
            $settings_value  = Settings::getValue($name, $default_value);
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'settings_value' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'settings_value' => $settings_value ], HTTP_RESPONSE_OK );
    } //public function get_logged_user_info()



    public function get_logged_user_info()
    {
        try {
            $loggedUser= '';
            if ( Auth::check() ) {
                $loggedUser = Auth::user();
                $loggedUser = User::getRowById($loggedUser->id, ['fill_labels'=> 1]);

            }
            $site_name  = Settings::getValue('site_name', '');

            $usersGroupsList= UsersGroups::getUsersGroupsList( ListingReturnData::LISTING, ['user_id'=> $loggedUser->id, 'show_group_name'=> 1 ] );
            $loggedUserInGroups= [];
            foreach( $usersGroupsList as $next_key=>$nextUsersGroup ) {
                $loggedUserInGroups[]= [ 'group_id'=> $nextUsersGroup->group_id, 'status'=> $nextUsersGroup->status, 'group_name'=> $nextUsersGroup->group_name ];
            }

        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'loggedUser' => '', 'loggedUserInGroups'=> null, 'site_name' => null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'loggedUser' => $loggedUser, 'loggedUserInGroups'=> $loggedUserInGroups, 'site_name'=> $site_name],
            HTTP_RESPONSE_OK );
    } //public function get_logged_user_info()


    /////////////////  USER WEATHER  BLOCK BEGIN //////////////////////
    public function user_weather_locations_dictionaries($user_weather_location_id)
    {
        try {
            $userWeatherLocationValueArraySelectionList   = UserWeatherLocation::getUserWeatherLocationValueArray();
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocationValueArraySelectionList'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'userWeatherLocationValueArraySelectionList' => $userWeatherLocationValueArraySelectionList ], HTTP_RESPONSE_OK );
    } // public function user_weather_locations_dictionaries($user_weather_location_id)



    public function user_weather_location_load()
    {
        $loggedUser      = Auth::user();
        $filtersArray    = [ 'user_id'=> $loggedUser->id, 'fill_labels'=>1 ];
        //tsk_user_chat_message_documents
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'ordering' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userWeatherLocationsList= UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction,
                $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userWeatherLocationsList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new UserWeatherLocation)->getItemsPerPage();

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userWeatherLocationsList'=>$userWeatherLocationsList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function user_weather_location_update(UserWeatherLocationRequest $request)  // OK
    {
        $loggedUser      = Auth::user();
        $id= $request->user_weather_location_id;
        $userWeatherLocation = UserWeatherLocation::find($id);
        if ( $userWeatherLocation == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s weather location # "'.$id.'" not found!', 'userWeatherLocation'=>(object)['name'=> 'User\'s weather location # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            $updateData= $request->all();
            $user_weather_location_count = UserWeatherLocation::getSimilarUserWeatherLocationByLocation( $updateData['location'], $loggedUser->id, $userWeatherLocation->id, true );
            if ($user_weather_location_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$updateData['location'].'" weather location is already taken!', 'userWeatherLocation'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            echo '<pre>$updateData::'.print_r($updateData,true).'</pre>';
            $userWeatherLocation->update($updateData);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'userWeatherLocation'=>$userWeatherLocation],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }

    public function user_weather_location_store(UserWeatherLocationRequest $request)
    {
        $loggedUser      = Auth::user();
        try {
            $insertData= $request->all();
            $user_weather_location_count = UserWeatherLocation::getSimilarUserWeatherLocationByLocation( $insertData['location'], $loggedUser->id, null, true );
            if ($user_weather_location_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$insertData['location'].'" weather location is already taken!', 'userWeatherLocation'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $insertData['user_id']= $loggedUser->id;
            $insertData['location_type']= 'C';
            if ( empty($insertData['ordering']) ) {
                $insertData['ordering']= UserWeatherLocation::getMaxOrdering();
            }
            $userWeatherLocation = UserWeatherLocation::create($insertData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'userWeatherLocation'=>$userWeatherLocation],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }



    public function user_weather_location_destroy($id)  // ok
    {
        try {
            $userWeatherLocation = UserWeatherLocation::find($id);
            if ( $userWeatherLocation == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s weather location # "'.$id.'" not found!', 'userWeatherLocation'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userWeatherLocation->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    public function get_weather_info_by_location($location)
    {
        $openweathermap_key= Config::get('app.OPENWEATHERMAP_KEY');
        if ( empty($openweathermap_key) ) {
            return response()->json( ['error_code'=> 1, 'message'=> 'Open weather map key is not set in application\'s environment', 'results'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $icon_base_url = '//openweathermap.org/img/w/$$.png';
        $query_url = "api.openweathermap.org/data/2.5/forecast?q=".$location."&appid=".$openweathermap_key.'&units=metric';
        $session = curl_init($query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($session);

        $weatherData = json_decode($json);
        if ( empty($weatherData->list) ) {
            return response()->json( ['error_code'=> 1, 'message'=> 'Invalid request for \'' . $location .'\' location', 'results'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $retWeatherDataArray= [];
        $index= 0;
        $prior_day_number= -1;
        $today_date_label= '';
        $dayNumbersArray= [];
//        echo '<pre>$weatherData->list::'.print_r($weatherData->list,true).'</pre>';
        foreach( $weatherData->list as $nextWeatherItem ) { // all weather rows
            $next_time_short_label= '';
            if ( !empty($weatherData->list[$index+1]) ) {
                $next_time_short_label= '-'.with(new UserWeatherLocation)->getFormattedDateTime( trim($weatherData->list[$index+1]->dt),  'H:i A' /* '%H:%M%p'*/ );
            }
//            echo '<pre>$next_time_short_label::'.print_r($next_time_short_label,true).'</pre>';
            $day_number= with(new UserWeatherLocation)->getDayNumberByTimestamp( trim( $nextWeatherItem->dt ) );

            if ($day_number == $prior_day_number+1) {
//                echo '<pre>$nextWeatherItem->dt::'.print_r($nextWeatherItem->dt,true).'</pre>';
//                echo '<pre>zzzz::'.print_r( strftime('%d %B %Y, %H:%M', $nextWeatherItem->dt),true).'</pre>';
//                $time_label = with(new UserWeatherLocation)->getFormattedDateTime(trim($nextWeatherItem->dt), 'd-m-Y'). $next_time_short_label;
                $time_label = strftime('%d %B %Y', $nextWeatherItem->dt). $next_time_short_label;
                //'Y-m-d H:i:s'
//                echo '<pre>-!!!!!!00 $time_label::'.print_r($time_label,true).'</pre>';
//                $date_label = with(new UserWeatherLocation)->getFormattedDateTime(trim($nextWeatherItem->dt), '%d %B, %A');
                $date_label = strftime( '%d %B, %A', trim($nextWeatherItem->dt) );
//                echo '<pre>-2 $date_label::'.print_r($date_label,true).'</pre>';
                $dayNumbersArray[]= [ 'key'=> $day_number, 'label' => $date_label ];
                if ($day_number == 0) {
                    $today_date_label = $date_label;
                }
            } else {
                $time_label = strftime('%d %B %Y', trim($nextWeatherItem->dt ) ). $next_time_short_label;
//                echo '<pre>-1 $time_label::'.print_r($time_label,true).'</pre>';
            }

//            die("-1 XXZ");
            $weatherItem= [
                'day_number'          => $day_number,
                'time'                => $nextWeatherItem->dt,
                'time_label'          => $time_label,
//                'time_short_label'    => with(new UserWeatherLocation)->getFormattedDateTime( trim($nextWeatherItem->dt), '%H:%M%p' ),
                'time_short_label'    => with(new UserWeatherLocation)->getFormattedDateTime( trim($nextWeatherItem->dt), 'H:i A' ),
                'temp'                => (int)ceil(  floatval($nextWeatherItem->main->temp)  ),
                'pressure'            => $nextWeatherItem->main->pressure, // Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
                'humidity'            => $nextWeatherItem->main->humidity, // Humidity, %
                'sea_level'           => $nextWeatherItem->main->sea_level, // Atmospheric pressure on the sea level, hPa
            ];

            $weatherItem['temp']= (int)ceil(  floatval($nextWeatherItem->main->temp) ); // Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.

            if ( !empty($nextWeatherItem->weather[0]) ) {
                $weatherItem['main_text']        = $nextWeatherItem->weather[0]->main;
                $weatherItem['main_description'] = $nextWeatherItem->weather[0]->description;
                $weatherItem['icon_url']         = 'http:'.str_replace( '$$', $nextWeatherItem->weather[0]->icon, $icon_base_url );
            }
            if ( !empty($nextWeatherItem->clouds->all) ) {
                $weatherItem['clouds']           = $nextWeatherItem->clouds->all;
            }

            if ( !empty($nextWeatherItem->snow->all) ) {
                $weatherItem['snow']            = $nextWeatherItem->snow->all;
            }

            if ( !empty($nextWeatherItem->snow->all) ) {
                $weatherItem['snow']            = $nextWeatherItem->snow->all;
            }
            $retWeatherDataArray[]= $weatherItem;
            $index++;
            $prior_day_number= $day_number;
        } // foreach( $weatherData->list as $nextWeatherItem ) { // all weather rows

        $cityInfoArray= [];
        if ( !empty($weatherData->city) ) {
            $cityInfoArray= [
                'city_name'=> $weatherData->city->name,
                'lat'=> $weatherData->city->coord->lat,
                'lon'=> $weatherData->city->coord->lon,
                'country'=> $weatherData->city->country,
                'country_label'=> $this->getCountryName( strtolower($weatherData->city->country) ),
            ];
        }

        return response()->json( ['error_code'=> 0, 'message'=> '', 'weatherDataArray'=> $retWeatherDataArray, 'cityInfoArray'=> $cityInfoArray, 'dayNumbersArray'=>
            $dayNumbersArray, 'today_date_label'=> $today_date_label ], HTTP_RESPONSE_OK);
    }

    ///////////////// USER WEATHER BLOCK END //////////////////////





    /////////////////  USER TODOS BLOCK BEGIN //////////////////////
    public function user_todos_dictionaries()
    {
        try {
            $taskPrioritiesSelectionList   = Task::getTaskPrioritiesValueArray(true);
            $tasksSelectionList            = Task::getTasksSelectionList();
            $userTodoCompletedValueArray   = UserTodo::getUserTodoCompletedValueArray();
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'taskPrioritiesSelectionList'=> null, 'tasksSelectionList'=> null, 'userTodoCompletedValueArray'=> null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'taskPrioritiesSelectionList' => $taskPrioritiesSelectionList, 'tasksSelectionList'=> $tasksSelectionList, 'userTodoCompletedValueArray'=> $userTodoCompletedValueArray ], HTTP_RESPONSE_OK );
    } // public function user_todos_dictionaries($user_todo_id)



    public function user_todos_load()
    {
        $loggedUser      = Auth::user();
        $filtersArray    = [ 'user_id'=> $loggedUser->id, 'fill_labels'=>1, 'show_task_name'=> 1 ];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'completed_priority' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= UserTodo::getUserTodosList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userTodosList= UserTodo::getUserTodosList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction,
                $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userTodosList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new UserTodo)->getItemsPerPage();

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userTodosList'=>$userTodosList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function user_todo_update(UserTodoRequest $request)
    {
        $loggedUser      = Auth::user();
        $id= $request->user_todo_id;
        $userTodo = UserTodo::find($id);

        if ( $userTodo == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s todo # "'.$id.'" not found!', 'userTodo'=>(object)['name'=> 'User\'s todo # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            $updateData= $request->all();
            DB::beginTransaction();
            $userTodo->update($updateData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'userTodo'=>$userTodo],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }

    public function user_todo_store(UserTodoRequest $request)
    {
        $loggedUser      = Auth::user();
        try {
            $insertData= $request->all();
            $user_todo_count = UserTodo::getSimilarUserTodoByText( $insertData['text'], $loggedUser->id, null, true );
            if ($user_todo_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$insertData['text'].'" todo is already taken!', 'userTodo'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $insertData['user_id']= $loggedUser->id;
            $insertData['priority']= 1;
            $insertData['completed']= false;
            $userTodo = UserTodo::create($insertData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> '', 'userTodo'=>$userTodo],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }

    public function user_todo_destroy($id)
    {
        try {
            $userTodo = UserTodo::find($id);
            if ( $userTodo == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s todo # "'.$id.'" not found!', 'userTodo'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userTodo->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }
    ///////////////// USER TODOS BLOCK END //////////////////////


    ///////////////// USER EVENTS BLOCK START //////////////////////
/*    public function get_user_events($user_id)
    {
        $filtersArray    = [ 'user_id'=> $user_id, 'fill_labels'=>1, 'show_task_name'=> 1, 'show_event_name'=> 1, 'set_former_at_time'=> 1 ];
        try { // https://github.com/Wanderxx/vue-fullcalendar
            $eventUsersArray= [];
            $eventUsersList= EventUser::getEventUsersList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray );
            foreach( $eventUsersList as $next_key=>$nextEventUser ) { // "event_at_time": "2018-02-08 17:30:00",
                $end_time= $this->addMinutesToFormattedDateTime($nextEventUser->event_at_time, $nextEventUser->event_duration);
                $eventUsersArray[]= [
                    'title'=> $nextEventUser->event_name,
                    'start'=> $nextEventUser->event_at_time,
                    'start_time_label'=> $this->getFormattedDateTime($nextEventUser->event_at_time, 'H:i A' ),
                    'end'=> $end_time,
                    'end_time_label'=> $this->getFormattedDateTime($end_time, 'H:i A' ),
                    'event_duration'=> $nextEventUser->event_duration,
                    'event_access'=> $nextEventUser->event_access,
                    'at_time_is_past'=> $nextEventUser->event_at_time_is_past,
                'cssClass' => [ $nextEventUser->event_at_time_is_past] , 'YOUR_DATA' => ['val'=>'Let it be.'] ];
            }
            
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'eventUsersList'=> null, 'eventUsersArray'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'eventUsersList' => $eventUsersList, 'eventUsersArray'=> $eventUsersArray ], HTTP_RESPONSE_OK );
    } // public function get_user_events($user_id)*/
    public function user_events_by_day_user_id($date, $user_id)
    {
//        echo '<pre>user_events_by_day_user_id $date::'.print_r($date,true).'</pre>';
//        echo '<pre>user_events_by_day_user_id $user_id::'.print_r($user_id,true).'</pre>';
//
        $day_is_past= $this->getDateTimesCompare($date);
        $begin_of_day= $this->getBeginOfDay($date, 'mysql', true);
//        echo '<pre>$begin_of_day::'.print_r($begin_of_day,true).'</pre>';

        $end_of_day= $this->getEndOfDay($date, 'mysql', true);
//        echo '<pre>$end_of_day::'.print_r($end_of_day,true).'</pre>';

        $view_day_formatted          =  $this->getFormattedDate($date, 'astext');
        $view_day_formatted_pickdate =  $this->getFormattedDateTime($date, 'Y-m-d');

//        echo '<pre>$view_day_formatted::'.print_r($view_day_formatted,true).'</pre>';
//        echo '<pre>$view_day_formatted_pickdate::'.print_r($view_day_formatted_pickdate,true).'</pre>';
//        die("-1 XXZ");
        $filtersArray= [ 'user_id'=> $user_id, 'at_time_start'=> $begin_of_day , 'at_time_end'=> $end_of_day ];
        $eventUsersByDayArray= [];
        $eventUsersList= EventUser::getEventUsersList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray );
        foreach( $eventUsersList as $next_key=>$nextEventUser ) { // "at_time": "2018-02-08 17:30:00",
            $end_time= $this->addMinutesToFormattedDateTime($nextEventUser->event_at_time, $nextEventUser->event_duration);
            $event_at_time_is_past= $nextEventUser->getDateTimesCompare($nextEventUser->event_at_time);
//            echo '<pre>$event_at_time_is_past::'.print_r($event_at_time_is_past,true).'</pre>';
            $eventUsersByDayArray[]= [
                'event_id'=> $nextEventUser->event_id,
                'title'=> $nextEventUser->event_name,
                'start'=> $nextEventUser->event_at_time,
                'start_time_label'=> $this->getFormattedDateTime($nextEventUser->event_at_time, 'H:i A' ),
                'end'=> $end_time,
                'end_time_label'=> $this->getFormattedDateTime($end_time, 'H:i A' ),
                'event_duration'=> $nextEventUser->event_duration,
                'event_access'=> $nextEventUser->event_access,
                'event_access_label'=> Event::getEventAccessLabel($nextEventUser->event_access),
//                'at_time_is_past'=> $nextEventUser->event_at_time_is_past,
                'event_at_time_is_past'=> $event_at_time_is_past,
                'event_class_name' => [ $event_at_time_is_past] , 'YOUR_DATA' => ['val'=>'Let it be.'] ];
        }
//        echo '<pre>$eventUsersByDayArray::'.print_r($eventUsersByDayArray,true).'</pre>';
//        die("-1 XXZ");
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'eventUsersByDayArray'=> $eventUsersByDayArray, 'view_day_formatted'=> $view_day_formatted,
                                   'view_day_formatted_pickdate'=> $view_day_formatted_pickdate, 'day_is_past'=> $day_is_past ], HTTP_RESPONSE_OK );

    } // public function user_events_by_day_user_id($user_id)*/


   public function recalc_user_events()
    {
        $events_added_count= 0;
        $event_users_added_count= 0;
        try {
            DB::beginTransaction();

            $events_to_add= 5;

            $usersList = User::getUsersList(ListingReturnData::LISTING, []);
            $usersArray= [];
            foreach( $usersList as $nextUser ) {
                $usersArray[]= $nextUser->id;
            }

            factory( \App\Event::class, $events_to_add)->create()->each(function($newEvent) use($usersArray, &$events_added_count, &$event_users_added_count ) {
                $events_added_count++;
                foreach( $usersArray as $next_key=>$next_user_id ) {
                    factory(\App\EventUser::class, 1)->create(['event_id' => $newEvent->id, 'user_id'=> $next_user_id]);
                    $event_users_added_count++;
                }
//                echo '<pre>$event_users_added_count::'.print_r($event_users_added_count,true).'</pre>';
            });

//            echo '<pre>$events_added_count::'.print_r($events_added_count,true).'</pre>';
//            echo '<pre>$event_users_added_count ::'.print_r($event_users_added_count,true).'</pre>';
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'events_added_count' => $events_added_count, 'event_users_added_count'=> $event_users_added_count ], HTTP_RESPONSE_OK );
    } // public function recalc_user_events()




    ///////////////// USER EVENTS BLOCK END //////////////////////

    public function get_source_file()
    {
        try {
            $src_file          =   $this->getParameter('src_file');
            $file_type         =   $this->getParameter('file_type');
            $src_file_path     =   base_path($src_file);
//            $src_file_path      =   ($src_file);
//            echo '<pre>$src_file_path::'.print_r($src_file_path,true).'</pre>';

            if (!File::exists($src_file_path))
            {
//                echo "No. No exists.";
                return response()->json(['error_code' => 1, 'message' => "File '".$src_file_path."' not found!", 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $file_content      =   File::get(base_path($src_file));
//            echo '<pre>EXISTS $file_content::'.print_r($file_content,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'src_file' => $src_file, 'file_type'=> $file_type, 'file_content'=> $file_content ],
            HTTP_RESPONSE_OK );
    } //public function get_source_file()

    public function get_system_info()
    {

//        $output = new Process('npm ls'); // http://symfony.com/doc/current/components/process.html

//        echo '<pre>$output::'.print_r($output,true).'</pre>';
//        dump($output);
        $npm_output_listing= '';
//        $output->run(function ($type, $buffer) {
//
//            if (Process::ERR === $type) {
//                ob_start();
//                echo 'ERR > '.$buffer;
//                $npm_output_listing = ob_get_contents();
//                ob_end_clean();
////                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';
////                die("-1 XXZ");
//            } else {
//                ob_start();
//                echo 'OUT > '.$buffer;
//                $npm_output_listing = str_replace(["\n","\r"],"",ob_get_contents());
//                ob_end_clean();
////                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';
////                die("-1 XXZ");
//            }
//            $_SESSION['npm_output_listing'] = str_replace(["\n","\r"],"",$npm_output_listing );
//        });

//        $npm_output_listing= !empty($_SESSION['npm_output_listing']) ? $_SESSION['npm_output_listing']: '';
//                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';

        try {
            $system_info             =   $this->getSystemInfo();
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'src_file' => null, 'npm_output_listing'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'system_info' => $system_info, 'npm_output_listing'=> $npm_output_listing ], HTTP_RESPONSE_OK );
    } //public function get_system_info()



    public function app_description()
    {
        $this->addVariablesToJS();
        return view('admin.app_description.index');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin/dashboard');
    }


}