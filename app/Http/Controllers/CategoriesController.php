<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\TaskAssignedToUser;
use App\User;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\Http\Requests\CategoryRequest;

class CategoriesController__DELETE extends MyAppController
{

    public function index()
    {
        $filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'name'=>'', 'show_task_assigned_to_users_count'=> 1];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'name' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= Category::getCategoriesList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $categoriesList= Category::getCategoriesList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'categoriesList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new Category)->getItemsPerPage();
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'categoriesList'=>$categoriesList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function show($id)  // OK
    {
        try {
            $category = Category::getRowById($id, ['fill_labels' => 1]);
            if ( $category == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Category # "'.$id.'" not found!', 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds', 2) );

        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category],HTTP_RESPONSE_OK);
    }


    public function update(CategoryRequest $request)  // OK
    {
        $id= $request->id;
        $category = Category::find($id);
        if ( $category == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Category # "'.$id.'" not found!', 'category'=>(object)['name'=> 'Category # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $category->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(CategoryRequest $request) // ok
    {
        try {
            DB::beginTransaction();
            $category = Category::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $category = Category::find($id);
            if ( $category == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Category # "'.$id.'" not found!', 'category'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $task_assigned_to_users_count= TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, [ 'category_id'=>$id ] );
            if ( $task_assigned_to_users_count > 0 ) {
                return response()->json(['error_code'=> 1, 'message'=> 'Category # "'.$category->name.'" can not be deleted, as it is used in 
                '.$task_assigned_to_users_count . ' user\'s assignment(s). ', 'category'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $category->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

}
