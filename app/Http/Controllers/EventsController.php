<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Event;
use App\User;
use App\Task;
use App\EventUser;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\Http\Requests\EventRequest;

class EventsController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $filtersArray= [ 'fill_labels'=>1, 'short_description'=>'', 'show_event_users_count'=>1, 'set_former_at_time'=> 1, 'show_task_name'=> 1 ];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'at_time' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= Event::getEventsList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $eventsList= Event::getEventsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
//            echo '<pre>$eventsList::'.print_r($eventsList,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'eventsList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new Event)->getItemsPerPage();

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'eventsList'=>$eventsList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }


    public function show($id)  // OK
    {
//        echo '<pre>== show $id::'.print_r($id,true).'</pre>';
        $id = (int)$id;
//        echo '<pre> show $id::'.print_r($id,true).'</pre>';
        try {
            $event = Event::getRowById($id, ['fill_labels' => 1, 'set_former_at_time'=>1 ]);
            if ($event == null) {
                return response()->json([
                    'error_code'                        => 11,
                    'message'                           => 'Event # "' . $id . '" not found!',
                    'event'                  => null,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK);
    }


    public function events_dictionaries($event_id)
    {

        try {
            $tasksSelectionList   = $this->setArrayHeader( [ '' => ' -Select Task- ' ], Task::getTasksSelectionList() );
            $eventAccessesSelectionList= Event::getEventAccessValueArray();
            $activeUsersSelectionList     = User::getUsersList(ListingReturnData::LISTING, ['status' => 'A'/*, 'task_assigned_to_users_by_task_id'=>$event_id*/  ]);

            $eventAssignedForUsersArray= [];
            $eventAssignedForUsersList= EventUser::getEventUsersList( ListingReturnData::LISTING, [ 'event_id'=>$event_id  ] );
            foreach( $eventAssignedForUsersList as $next_key=>$nextEventAssignedForUser ) {
                $eventAssignedForUsersArray[]= $nextEventAssignedForUser->user_id;
            }

            $activeUsersSelectionArray= [];
            foreach( $activeUsersSelectionList as $next_key=>$nextActiveUsersSelection ) {
                $nextActiveUsersSelection= $nextActiveUsersSelection->toArray();
                $nextActiveUsersSelection['is_checked']= false;
                reset($eventAssignedForUsersArray);
                foreach ($eventAssignedForUsersArray as $next_assigned_to_user_key => $next_assigned_to_user_id) {
                    if ( ( (int)$nextActiveUsersSelection['id'] ) == ( (int)$next_assigned_to_user_id ) ) { // found assigned user
                        $nextActiveUsersSelection['is_checked']= true;
                        break;
                    }
                }
                $activeUsersSelectionArray[]= $nextActiveUsersSelection;
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null, 'eventTypeSelectionList'=> null, 'tasksSelectionList' => null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'eventAccessesSelectionList'=> $eventAccessesSelectionList, 'eventAssignedForUsersArray'=>
            $eventAssignedForUsersArray, 'activeUsersSelectionArray'=> $activeUsersSelectionArray, 'tasksSelectionList' => $tasksSelectionList], HTTP_RESPONSE_OK );
    } // public function events_dictionaries($event_id)


    public function update(EventRequest $request)  // OK
    {
        $id= $request->id;
        $event = Event::find($id);
        if ( $event == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Event # "'.$id.'" not found!', 'event'=>(object)['name'=> 'Event # "'.$id .'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();

            $updateDataArray = $request->all();
//            echo '<pre>==$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
//            echo '<pre>++$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            $eventUsersArray= ( !empty($updateDataArray['eventUsersArray']) and is_array($updateDataArray['eventUsersArray']) ) ? $updateDataArray['eventUsersArray'] : [];
            unset($updateDataArray['eventUsersArray']);

            $event->update($updateDataArray);
            $event->eventUsers()->delete();
            foreach ($eventUsersArray as $next_key => $next_user_id) {
                $newEventUser       = EventUser::create([
                    'user_id'      => $next_user_id,
                    'event_id' => $id
                ]);
                $new_event_user_id = $newEventUser->id;
//                echo '<pre>$new_event_user_id::'.print_r($new_event_user_id,true).'</pre>';
            }


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(EventRequest $request)
    {
        try {
            DB::beginTransaction();

            $insertDataArray = $request->all();
            $eventUsersArray= ( !empty($insertDataArray['eventUsersArray']) and is_array($insertDataArray['eventUsersArray']) ) ? $insertDataArray['eventUsersArray'] : [];
            unset($insertDataArray['eventUsersArray']);

            $event = Event::create($insertDataArray);
            foreach ($eventUsersArray as $next_key => $next_user_id) {
                $newEventUser       =  EventUser::create([
                    'user_id'       => $next_user_id,
                    'event_id'      => $event->id
                ]);
                $new_event_user_id = $newEventUser->id;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $event = Event::find($id);
            if ( $event == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Event # "'.$id.'" not found!', 'event'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();

            $event->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    public function load_related_events_list(string $request_type, int $filter_id)
    {
        $filterArray= [ 'fill_labels' => 1, 'set_former_at_time'=> 1 ];
        if ( $request_type == 'by_task_id' ) {
            $filterArray['task_id']= $filter_id;
        }
        $relatedEventsList = Event::getEventsList(ListingReturnData::LISTING, $filterArray, 'e.at_time', 'desc' );
        return response()->json( ['error_code' => 0, 'message' => '', 'relatedEventsList' => $relatedEventsList], HTTP_RESPONSE_OK );
    } // public function public function load_related_events_list(string $request_type, int $filter_id)


}
