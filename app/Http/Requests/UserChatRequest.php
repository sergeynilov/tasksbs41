<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use App\UserChat;


class UserChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $dataArray= $request->all();
//        echo '<pre>::'.print_r($dataArray,true).'</pre>';
        return UserChat::getValidationRulesArray( $request->get('id'), true );
    }
}
