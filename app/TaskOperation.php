<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\TaskAssignedToUser;
use App\library\ListingReturnData;

class TaskOperation extends MyAppModel
{

    protected $fillable = [ 'user_id',  'status', 'is_leader', 'task_id', 'user_task_type_id', 'description' ];


    protected $table = 'task_operations';
    protected $primaryKey = 'id';
    public $timestamps = false;        
    private static $taskOperationStatusLabelValueArray = Array('D' => 'Draft', 'A' => 'Assigning', 'C' => 'Cancelled', 'P' => 'Processing', 'K' => 'Checking', 'O' => 'Completed');


    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }
    
    public function task(){
        return $this->belongsTo('App\Task', 'task_id','id');
    }

    public static function getTaskOperationStatusValueArray($key_return= true, $statusLimitArray=[]) : array
    {
        $resArray = [];
        foreach (self::$taskOperationStatusLabelValueArray as $key => $value) {
            if ( !empty($statusLimitArray) ) {
                if ( !in_array($key, $statusLimitArray) ) continue;
            }
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getTaskOperationStatusLabel(string $status):string
    {
        if (!empty(self::$taskOperationStatusLabelValueArray[$status])) {
            return self::$taskOperationStatusLabelValueArray[$status];
        }
        return '';
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getTaskOperationsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'too.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $task_operation_table_name= with(new TaskOperation)->getTableName();
        $quoteModel= TaskOperation::from(  \DB::raw(DB::getTablePrefix().$task_operation_table_name.' as too' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'too.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( \DB::raw('too.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['task_id'])) {
            $quoteModel->where( \DB::raw('too.task_id'), '=', $filtersArray['task_id'] );
        }

        if (!empty($filtersArray['task_assigned_to_user_id'])) {
            $quoteModel->where( \DB::raw('too.task_assigned_to_user_id'), '=', $filtersArray['task_assigned_to_user_id'] );
        }

        if (!empty($filtersArray['status'])) {
            $quoteModel->where( \DB::raw('too.status'), '=', $filtersArray['status'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( \DB::raw("too.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( \DB::raw("too.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u.name as username, u.first_name, u.last_name, u.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('too.user_id') );
        } // if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item


        if ( !empty($filtersArray['user_operation_id'])  ) { // need to join in select sql username and user_status field of author of t item
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', uo.name as task_operation_username, uo.first_name as task_operation_first_name, uo.last_name as task_operation_first_name, uo.status as task_operation_user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as uo '), \DB::raw('uo.id'), '=', \DB::raw('too.user_operation_id') );
        } // if ( !empty($filtersArray['user_operation_id'])  ) { // need to join in select sql username and user_status field of author of t item


        if ( !empty($filtersArray['show_tasks_info'])  ) { // need to join in select sql task name
            $tasks_table_name= DB::getTablePrefix() . ( with(new Task)->getTableName() );
            $additive_fields_for_select .= ', t.name as task_name, t.status as task_status, t.date_start as task_date_start, t.date_end as task_date_end, t.priority as task_priority' ;
            $quoteModel->join( \DB::raw($tasks_table_name . ' as t '), \DB::raw('t.id'), '=', \DB::raw('too.task_id') );
        } // if ( !empty($filtersArray[''show_tasks_info''])  ) { // need to join in select sql username and user_status field of author of t item



        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new TaskOperation)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new TaskOperation)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $taskOperationsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $taskOperationsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $taskOperationsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $taskOperationsList as $next_key=>$nextTaskOperation ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextTaskOperation['status_label']= with(new TaskOperation)->getTaskOperationStatusLabel($nextTaskOperation->status);
                $nextTaskOperation['prior_status_label']= with(new TaskOperation)->getTaskOperationStatusLabel($nextTaskOperation->prior_status);
                $nextTaskOperation['is_leader_label']= $nextTaskOperation->is_leader ? 'Task Leader' : '';
                $nextTaskOperation['created_at_label']= with(new TaskOperation)->getFormattedDateTime($nextTaskOperation->created_at);
                if ( !empty($filtersArray['show_tasks_info'])  ) { // need to join in select sql task name
                    $nextTaskOperation['task_task_priority_label'] = with(new Task)->getTaskPriorityLabel($nextTaskOperation->task_priority);
                    $nextTaskOperation['task_date_start_label']    = with(new TaskOperation)->getFormattedDate($nextTaskOperation->task_date_start);
                    $nextTaskOperation['task_date_end_label']      = with(new TaskOperation)->getFormattedDate($nextTaskOperation->task_date_end);
                }
                if ( !empty($nextTaskOperation['user_status']) ) {
                    $taskOperationsList[$next_key]['user_status_label'] = User::getUserStatusLabel($nextTaskOperation->user_status);
                }
                if ( !empty($nextTaskOperation['task_status']) ) {
                    $taskOperationsList[$next_key]['task_status_label'] = Task::getTaskStatusLabel($nextTaskOperation->task_status);
                }
            }
        }
        return $taskOperationsList;

    } // public static function getTaskOperationsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if ( !$is_row_retrieved ) {
            $taskOperation = TaskOperation::find($id);
        }

        if (empty($taskOperation)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $taskOperation['created_at_label']= with(new TaskOperation)->getFormattedDateTime($taskOperation->created_at);
        }
        return $taskOperation;
    } // public function getRowById( int $id, array $additiveParams= [] )



    public static function getValidationRulesArray($task_operation_id) : array
    {
        $validationRulesArray = [
            'user_id'                         => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'task_id'                         => 'required|exists:'.( with(new Task)->getTableName() ).',id',
            'task_assigned_to_user_id'        => 'required|exists:'.( with(new TaskAssignedToUser)->getTableName() ).',id',
            'prior_status'                    => 'required|in:'.with( new TaskAssignedToUser)->getValueLabelKeys( TaskAssignedToUser::getTaskAssignedToUserStatusValueArray (false) ),
            'status'                          => 'required|in:'.with( new TaskAssignedToUser)->getValueLabelKeys( TaskAssignedToUser::getTaskAssignedToUserStatusValueArray (false) ),
            'info'                            => 'nullable',
        ];
        return $validationRulesArray;
    }

}

