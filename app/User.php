<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\library\ListingReturnData;
use App\MyAppModel;
use App\Group;
use App\UsersGroups;
use App\TaskAssignedToUser;
use App\UserChatNewMessage;
use App\Http\Traits\funcsTrait;

class User extends Authenticatable
{
    use Notifiable;
    use funcsTrait;
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'name', 'email', 'first_name', 'last_name', 'phone', 'website', 'password' ];

    protected $hidden = [ 'password', 'remember_token'  ];

    private static $userStatusLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive', 'N' => 'New');

    public function getTableName() : string
    {
        return $this->table;
    }

    public function getPrimaryKey() : string
    {
        return $this->primaryKey;
    }

    public function userChatMessages()
    {
        return $this->hasMany('App\UserChatMessage');
    }

   public function userChatNewMessages()
    {
        return $this->hasMany('App\UserChatNewMessage');
    }


    public static function getUserStatusValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$userStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getUserStatusLabel(string $status):string
    {
        if (!empty(self::$userStatusLabelValueArray[$status])) {
            return self::$userStatusLabelValueArray[$status];
        }
        return '';
    }



    public static function getUsersGroupsByUserId(int $user_id, $show_group_title= false)
    {
        $usersGroupsList= DB::table('users_groups AS gr')  ->where( 'gr.user_id', $user_id )     ->get();
        if ( !$show_group_title ) return $usersGroupsList;
        return $usersGroupsList;
    }


    /* return data array by keys id/name based on filters/ordering... */
    public static function getUsersSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $usersList = User::getUsersList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($usersList as $nextUser) {
            if ($key_return) {
                $resArray[] = array('key' => $nextUser->id, 'label' => $nextUser->name . ' (' . $nextUser->email . ')');
            } else{
                $resArray[ $nextUser->id ]= $nextUser->name . ' (' . $nextUser->email . ')';
            }
        }
        return $resArray;
    }


    public function getFirstNameAttribute($value) {
        return ucwords($value);
    }
//    public function setFirstNameAttribute($value) {
//        return $this->attribute['first_name'];
//    }

    public function getLastNameAttribute($value) {
        return ucwords($value);
    }
//    public function setLastNameAttribute($value) {
//        return $this->attribute['last_name'];
//    }

    /* return data array by keys id/title based on filters/ordering... */
    public static function getUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'name';  // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';

        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';
        $user_table_name= DB::getTablePrefix().with(new User)->getTableName();
        $groups_table_name= DB::getTablePrefix().( with(new Group)->getTableName() );
        $users_groups_table_name= DB::getTablePrefix().( with(new UsersGroups)->getTableName() );
        $quoteModel= User::from(  \DB::raw($user_table_name.' as u' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'u.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            $quoteModel->whereRaw( MyAppModel::pgStrLower('name', false, false) . ' like ' . MyAppModel::pgStrLower( $filtersArray['name'], true,true ));
        }

//        if ( !empty($filtersArray['show_cms_items_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $cms_item_table_name . ' as ci where ci.user_id = ' . \DB::raw('u.id').' ) as cms_items_count';
//        }

        $is_user_group_joined= false;
        if ( !empty($filtersArray['show_user_groups']) and $listingReturnData != ListingReturnData::ROWS_COUNT ) {
            $additive_fields_for_select .= ", array_to_string(array_agg(  g.name  ORDER BY g.name   ),',' ) as group_names ";
            if ( !$is_user_group_joined ) {
                $quoteModel->join( \DB::raw($users_groups_table_name . ' as ug '), \DB::raw('ug.user_id'), '=', \DB::raw('u.id') );
                $quoteModel->join( \DB::raw($groups_table_name . ' as g '), \DB::raw('g.id'), '=', \DB::raw('ug.group_id') );
            }
            $is_user_group_joined= true;
        }


/*        $is_task_assigned_to_users_joined= false;
        if ( !empty($filtersArray['task_assigned_to_users_by_task_idXXX']) ) {
            $task_assigned_to_user_table_name= DB::getTablePrefix().with(new TaskAssignedToUser)->getTableName();
//            $additive_fields_for_select .= ", array_to_string(array_agg(  g.name  ORDER BY g.name   ),',' ) as group_names ";
            if ( !$is_task_assigned_to_users_joined ) {
                $quoteModel->join( \DB::raw($task_assigned_to_user_table_name . ' as tau '), \DB::raw('tau.user_id'), '=', \DB::raw('u.id') );
                $quoteModel->whereRaw('tau.task_id = ' . $filtersArray['task_assigned_to_users_by_task_id']);
//                $quoteModel->join( \DB::raw($user_table_name . ' as u1 '), \DB::raw('u1.id'), '=', \DB::raw('tau.user_id') );
//                $quoteModel->join( \DB::raw($task_assigned_to_user_table_name . ' as tau '), \DB::raw('tau.task_id'), '=', $filtersArray['task_assigned_to_users_by_task_id'] );
//                $quoteModel->join( \DB::raw($user_table_name . ' as u1 '), \DB::raw('u1.id'), '=', \DB::raw('tau.user_id') );
            }
            $is_task_assigned_to_users_joined= true;
        }
*/



        if (!empty($filtersArray['status'])) {
            $quoteModel->where( \DB::raw('u.status'), '=', $filtersArray['status'] );
        }



        if (!empty($filtersArray['in_groups'])) {
            $groupsArray= [];
            if (   is_string($filtersArray['in_groups'])   ) {
                $groupsArray= with(new User)->pregSplit('/,/',$filtersArray['in_groups']);
            }
            if (   is_array($filtersArray['in_groups'])   ) {
                $groupsArray= $filtersArray['in_groups'];
            }
            if ( !$is_user_group_joined ) {
                $quoteModel->join(\DB::raw($users_groups_table_name . \DB::raw(' as ug ')), \DB::raw('ug.user_id'), '=', \DB::raw('u.id'));
            }
            $quoteModel->whereIn( \DB::raw('ug.group_id'), $groupsArray );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->where( \DB::raw('created_at'), '>=', "'".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->where( \DB::raw('created_at'), '<=', "'".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( $listingReturnData != ListingReturnData::ROWS_COUNT ) {
            $quoteModel->groupBy( \DB::raw('u.id') );
        }


        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $product_user_table_name= DB::getTablePrefix().( with(new ProductUser)->getTableName() );
//        if ( !empty($filtersArray['show_products_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $product_user_table_name . ' as pc where pc.user_id = ' . 'u.id ) as user_products_count';
//        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of u table */
        $items_per_page= with(new Group)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new User)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) { /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $usersList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $usersList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }
        if ( !$data_retrieved ) {
            $usersList = $quoteModel->get();
            $data_retrieved= true;
        }
//        die("-1 XXZ");
        foreach( $usersList as $next_key=>$nextUser ) { // /* map all retrieved data when need to set human readable labels for some fields */

            if (!empty($filtersArray['fill_labels'])) {
                $nextUser['created_at_label']= with(new User)->getFormattedDateTime($nextUser->created_at);
                $nextUser['updated_at_label']= with(new User)->getFormattedDateTime($nextUser->updated_at);
                    $nextUser['contract_start_label']= with(new User)->getFormattedDate($nextUser->contract_start);
                    $nextUser['contract_end_label']= with(new User)->getFormattedDate($nextUser->contract_end);
                    $nextUser['status_label']= with(new User)->getUserStatusLabel($nextUser->status);
                }
/*   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                `contract_start` date DEFAULT NULL,
  `contract_end` date DEFAULT NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`), */
//            if ( !empty('show_user_gravatar') ) {
//                $usersList[$next_key]['user_gravatar_url']= with(new User)->getGravatarHtml( $nextUser->email, 32 );
//
//            }
        }
        return $usersList;

    } // public static function getUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $user = User::find($id);
            if ( $user == null ) return false;
            if (!empty($additiveParams['fill_labels'])) {
                $user['created_at_label']= with(new User)->getFormattedDateTime($user->created_at);
                if (!empty($user->contract_start)) {
                    $user['contract_start_label'] = with(new User)->getFormattedDate($user->contract_start);
                }
                if ( !empty($user->contract_end) ) {
                    $user['contract_end_label'] = with(new User)->getFormattedDate($user->contract_end);
                }
                $user['status_label']= with(new User)->getUserStatusLabel($user->status);
            }
        }

        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $user= with(new User)->substituteNullValues($user, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $user;
    } // public function getRowById( int $id, array $additiveParams= [] )
    
}
