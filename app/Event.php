<?php

namespace App;

use DB;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\MyAppModel;
use App\EventUser;
use App\library\ListingReturnData;
use Carbon\Carbon;
use App\Rules\CheckEventUserSelected;

class Event extends MyAppModel
{

    protected $table = 'events';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $eventAccessLabelValueArray = Array('P' => 'Private', 'U' => 'Public');
    protected $fillable = [
        'name', 'access', 'at_time', 'duration', 'task_id', 'description'
    ];

    public function eventUsers()
    {
        return $this->hasMany('App\EventUser');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($user) {
            $user->eventUsers()->delete();
        });
    }


    public static function getEventAccessValueArray($key_return= true, $statusLimitArray=[]) : array
    {
        $resArray = [];
        foreach (self::$eventAccessLabelValueArray as $key => $value) {
            if ( !empty($statusLimitArray) ) {
                if ( !in_array($key, $statusLimitArray) ) continue;
            }
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getEventAccessLabel(string $status):string
    {
        if (!empty(self::$eventAccessLabelValueArray[$status])) {
            return self::$eventAccessLabelValueArray[$status];
        }
        return '';
    }

    /* return data array by keys id/name based on filters/ordering... */
    public static function getEventsSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $eventsList = Event::getEventsList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($eventsList as $nextEvent) {
            if ($key_return) {
                $resArray[] = array('key' => $nextEvent->id, 'label' => $nextEvent->name);
            } else{
                $resArray[ $nextEvent->id ]= $nextEvent->name;
            }
        }
        return $resArray;
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getEventsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'e.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $event_table_name= with(new Event)->getTableName();
        $quoteModel= Event::from(  \DB::raw(DB::getTablePrefix().$event_table_name.' as e' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'e.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_content']) ) {
                $quoteModel->whereRaw( Event::myStrLower('name', false, false) . ' like ' . Event::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Event::myStrLower('name', false, false) . ' like ' . Event::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . Event::myStrLower('content', false, false) . ' like ' . Event::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Event::myStrLower('description', false, false) . ' like ' . Event::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['access'])) {
            $quoteModel->where( 'access', '=', $filtersArray['access'] );
        }

        if (!empty($filtersArray['user_id'])) {
            $event_user_table_name= DB::getTablePrefix().with(new EventUser)->getTableName();
            $quoteModel->join(\DB::raw($event_user_table_name . \DB::raw(' as eu ')), \DB::raw('eu.event_id'), '=', \DB::raw('e.id'));
            $quoteModel->where( DB::raw('eu.user_id'), '=', $filtersArray['user_id'] );
        }

        $event_user_table_name= DB::getTablePrefix().( with(new EventUser)->getTableName() );
        if ( !empty($filtersArray['show_event_users_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $event_user_table_name . ' as eu where eu.event_id = ' . 'e.id ) as event_users_count ';
        }

        if (!empty($filtersArray['task_id'])) {
            $quoteModel->where( 'task_id', '=', $filtersArray['task_id'] );
        }

        if (!empty($filtersArray['only_future'])) {
            $today= Carbon::today()->format('Y-m-d');
            $quoteModel->whereRaw( "at_time >'".$today . " 23:59:59'" );
        }
        if (!empty($filtersArray['only_today'])) {
            $today= Carbon::today()->format('Y-m-d');
            $quoteModel->whereRaw( "at_time >='".$today . " 00:00:00' and at_time <= '".$today . " 23:59:59'" );
        }
        if (!empty($filtersArray['only_tomorrow'])) {
            $tomorrow = Carbon::tomorrow('Europe/London')->format('Y-m-d');
            $quoteModel->whereRaw( "at_time >='".$tomorrow . " 00:00:00' and at_time <= '".$tomorrow . " 23:59:59'" );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_task_name'])  ) { // need to join in select sql task name and task_status field
            $tasks_table_name= DB::getTablePrefix() . ( with(new Task)->getTableName() );
            $additive_fields_for_select .= ', t.name as task_name, t.status as task_status' ;
            $quoteModel->leftJoin( \DB::raw($tasks_table_name . ' as t '), \DB::raw('t.id'), '=', \DB::raw('e.task_id') );
        } // if ( !empty($filtersArray['show_task_name'])  ) { // need to join in select sql task name and task_status field

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Event)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Event)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $eventsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $eventsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $eventsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $eventsList as $next_key=>$nextEvent ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['set_former_at_time'])) {
                $nextEvent['at_time_is_past']= with(new Event)->getDateTimesCompare( $nextEvent->at_time, time() );
            }
            if (!empty($filtersArray['fill_labels'])) {
                $nextEvent['at_time_label']= with(new Event)->getFormattedDateTime($nextEvent->at_time);
                $nextEvent['created_at_label']= with(new Event)->getFormattedDateTime($nextEvent->created_at);
                $nextEvent['access_label']= with(new Event)->getEventAccessLabel($nextEvent->access);
            }
        }
        return $eventsList;

    } // public static function getEventsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_task_name']) ) { // need to join in select sql username and user_status field of author of t item
            $additive_fields_for_select= "";
            $fields_for_select= 'e.*';
            $event_table_name= with(new Event)->getTableName();
            $quoteModel= Event::from(  \DB::raw(DB::getTablePrefix().$event_table_name.' as t' ));
            $quoteModel->where( \DB::raw('e.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', uc.name as creator_name, uc.last_name as creator_last_name, uc.first_name as creator_first_name, uc.status as creator_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as uc '), \DB::raw('uc.id'), '=', \DB::raw('e.creator_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $eventRows = $quoteModel->get();
            if (!empty($eventRows[0]) and get_class($eventRows[0]) == 'App\Event' ) {
                $event= $eventRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_task_name']) ) { // need to join in select sql username and user_status field of author of t item

        if ( !$is_row_retrieved ) {
            $event = Event::find($id);
            if (empty($event)) return false;
        }
        if (!empty($additiveParams['set_former_at_time'])) {
            $event['at_time_is_past']= with(new Event)->getDateTimesCompare( $event->at_time, time() );
        }

        if (!empty($additiveParams['fill_labels'])) {
            $event['at_time_label']= with(new Event)->getFormattedDateTime($event->at_time);
            $event['created_at_label']= with(new Event)->getFormattedDateTime($event->created_at);
            $event['access_label']= with(new Event)->getEventAccessLabel($event->access);
        }

        if (empty($event)) return false;

        return $event;
    } // public function getRowById( int $id, array $additiveParams= [] )

    /* check if provided name is unique for events.name field */
    public static function getSimilarEventByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Event::whereRaw( Event::myStrLower('name', false, false) .' = '. Event::myStrLower( Event::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getValidationRulesArray( $event_id ) : array
    {
        $additional_name_validation_rule= 'check_event_unique_by_name:'.( !empty($event_id)?$event_id:'');
        $validationRulesArray = [
            'name'             => 'required|max:255|'.$additional_name_validation_rule,
            'access'           => 'required|in:'.with( new Event)->getValueLabelKeys(  Event::getEventAccessValueArray(false) ),
            'at_time'          => 'required',
            'duration'         => 'required|integer|min:1|max:6000',
            'task_id'          => 'nullable|exists:'.( with(new Task)->getTableName() ).',id',
            'description'      => 'required',
            'eventUsersArray'  => [ '',new CheckEventUserSelected() ]
        ];

        return $validationRulesArray;
    }

}