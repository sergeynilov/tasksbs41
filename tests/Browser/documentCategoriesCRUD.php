<?php

namespace Tests\Browser;

use Auth;
use DB;

use App\User;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome;

use Illuminate\Foundation\Testing\DatabaseMigrations;

//use PhpUnit\Framework\Assert; // vendor/phpunit/phpunit/src/Framework/Assert.php
use PhpUnit\Framework; // vendor/phpunit/phpunit/src/Framework/Assert.php
use Tests\TestCase;
use App\Settings;

use App\DocumentCategory;
use App\library\ListingReturnData;


class documentCategoriesCRUDTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testDocumentCategoriesCRUD()   //  runDocumentCategoriesCRUD php artisan dusk  tests/Browser/documentCategoriesCRUD.php
    {
        $logged_user_id = 5;
        $site_name = Settings::getValue('site_name', '');

        $info= 'debugging string';

        try {
//            DB::beginTransaction();    // php artisan dusk  tests/Browser/documentCategoriesCRUD.php

            $items_per_page= Settings::getValue( 'items_per_page', 0 );
            Settings::updateSettings( ['items_per_page'=> 0 ] );

            $document_category_id= '';
            $this->browse(function (Browser $browser) use ($site_name, $document_category_id, $logged_user_id) {
//                echo '<pre>$logged_user_id::'.print_r($logged_user_id,true).'</pre>';
                $browser->resize(1920, 1080); // Width and Height
                $document_categories_rows_count= DocumentCategory::getDocumentCategoriesList( ListingReturnData::ROWS_COUNT, [] );

//                echo '<pre>$document_categories_rows_count::'.print_r($document_categories_rows_count,true).'</pre>';
                $new_document_category_name = 'new document category created  at ' . time();

                $loggedUser= User::find($logged_user_id);
                echo '$loggedUser->name::'.print_r($loggedUser->name,true);
                $browser->loginAs(User::find($logged_user_id))
//                        ->visit('/admin/dashboard#/admin/document_categories/edit/new')
                        ->visit('http://local-tasksbs41.com/admin/dashboard#/admin/document_categories/edit/new')
//                        ->assertStatus(200)
                        ->type('#name', $new_document_category_name)// Поле заполняется ->type , но НЕ заполняется если ->value
//                        ->select('#type', 'D')// Enum field
//                        ->type('#document_category_content', $new_document_category_name . ' content lorem...')// textarea input
//                        ->click('.editor_button_submit')
//                        ->waitUntilMissing('.editor_button_submit')
//                        ->assertTitle('Document Categories of ' . $site_name)


//                    ->assertEquals( DocumentCategory::getDocumentCategoriesList( ListingReturnData::ROWS_COUNT, [] ) + 1, $document_categories_rows_count)
//                        ->assertPathIs('/admin/dashboard#/admin/document_categories')
                ;
// <span id="span_document_category_name_13">new document category created at 1523285454</span>
/* assertValue
Assert the element matching the given selector has the given value:

$browser->assertValue($selector, $value); */

//                $newDocumentCategory = DocumentCategory::getSimilarDocumentCategoryByName( $new_document_category_name, 0, false );
//                if ($newDocumentCategory !== null) {
//                    $browser->loginAs(User::find($logged_user_id))
//                            ->visit('/admin/dashboard#/admin/document_categories/edit/'.$newDocumentCategory->id)
////                            ->assertStatus(200)
//                            ->type('#name', $new_document_category_name)// Поле заполняется ->type , но НЕ заполняется если ->value
//                            ->select('#type', 'D')// Enum field
//                            ->type('#document_category_content', $new_document_category_name . ' content lorem...')// textarea input
//                            ->click('.editor_button_submit')
//                            ->waitUntilMissing('.editor_button_submit')
//                            ->assertTitle('Document Categories of ' . $site_name)
//                    ;
//                }

                $document_category_id= DB::getPdo()->lastInsertId();
                echo ' New document category id::'.print_r($document_category_id,true);


            });

            Settings::updateSettings( ['items_per_page'=> $items_per_page ] );

            echo ' New document category id::'.print_r($document_category_id,true);

        } catch (Exception $e) {

//            DB::rollBack();
            throw $e;
        }
//        DB::commit();

        return;

    }
}
