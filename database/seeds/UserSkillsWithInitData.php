<?PHP

use Illuminate\Database\Seeder;

class UserSkillsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('user_skills')->insert([
            'user_id' => 1,
            'skill' => 'PHP',
            'rating'=>8,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 1,
            'skill' => 'Laravel',
            'rating'=>6,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 1,
            'skill' => 'Vue.js',
            'rating'=>0,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 1,
            'skill' => 'HTML',
            'rating'=>7,
        ]);

        DB::table('user_skills')->insert([
            'user_id' => 1,
            'skill' => 'Team leader',
            'rating'=>8,
        ]);


        DB::table('user_skills')->insert([
            'user_id' => 2,
            'skill' => 'PHP',
            'rating'=>6,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 2,
            'skill' => 'Laravel',
            'rating'=>5,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 2,
            'skill' => 'Vue.js',
            'rating'=>2,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 2,
            'skill' => 'HTML',
            'rating'=>9,
        ]);



        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'PHP',
            'rating'=>4,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'Laravel',
            'rating'=>0,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'Vue.js',
            'rating'=>7,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'Bootstrap',
            'rating'=>9,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'HTML',
            'rating'=>10,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 3,
            'skill' => 'Remote administrator',
            'rating'=>8,
        ]);

        DB::table('user_skills')->insert([
            'user_id' => 4,
            'skill' => 'PHP',
            'rating'=>6,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 4,
            'skill' => 'Laravel',
            'rating'=>5,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 4,
            'skill' => 'Vue.js',
            'rating'=>2,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 4,
            'skill' => 'Bootstrap',
            'rating'=>8,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 4,
            'skill' => 'HTML',
            'rating'=>9,
        ]);

        DB::table('user_skills')->insert([
            'user_id' => 5,
            'skill' => 'PHP',
            'rating'=>8,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 5,
            'skill' => 'Laravel',
            'rating'=>3,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 5,
            'skill' => 'Vue.js',
            'rating'=>0,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 5,
            'skill' => 'Bootstrap',
            'rating'=>2,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 5,
            'skill' => 'HTML',
            'rating'=>7,
        ]);

        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'PHP',
            'rating'=>6,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'Laravel',
            'rating'=>2,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'Vue.js',
            'rating'=>2,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'Bootstrap',
            'rating'=>5,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'HTML',
            'rating'=>4,
        ]);
        DB::table('user_skills')->insert([
            'user_id' => 6,
            'skill' => 'Tester',
            'rating'=>7,
        ]);
    }
}
