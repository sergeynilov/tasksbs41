<?php

use Illuminate\Database\Seeder;

class DocumentCategoriesWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        try {

            DB::table('document_categories')->insert([
                'name'        => 'Profile main image',
                'type'        => 'M',
                'description' => 'Profile main image description...',
            ]);

            DB::table('document_categories')->insert([
                'name'        => 'Profile thumbnail image',
                'type'        => 'T',
                'description' => 'Profile thumbnail image description...',
            ]);

            DB::table('document_categories')->insert([
                'name'        => 'User\'s documents (diploma, certificate, license)',
                'type'        => 'P',
                'description' => 'User\'s documents (diploma, certificate, license) description...',
            ]);


            DB::table('document_categories')->insert([
                'name'        => 'Task\'s specification documents',
                'type'        => 'D',
                'description' => 'Task\'s specification documents description...',
            ]);

            DB::table('document_categories')->insert([
                'name'        => 'Task\'s attached images',
                'type'        => 'D',
                'description' => 'Task\'s attached images description...',
            ]);

            DB::table('document_categories')->insert([
                'name'        => 'Task\'s attached schemes',
                'type'        => 'D',
                'description' => 'Task\'s attached schemes description...',
            ]);

            DB::table('document_categories')->insert([
                'name'        => 'Chat\'s attached files',
                'type'        => 'C',
                'description' => 'Chat\'s attached files description...',
            ]);


        } catch (Exception $e) {
            throw $e;
        }

    }
}
