<?php

use Illuminate\Database\Seeder;

class user_todosWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_todos')->insert([
            'user_id'  => 5,
            'text'     => 'To do line Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut 1...',
            'priority' => 4,
            'task_id'  => 1,
            'completed'=> false,
        ]);

        DB::table('user_todos')->insert([
            'user_id'  => 5,
            'text'     => 'To do line 2...',
            'priority' => 3,
            'task_id'  => null,
            'completed'=> true,
        ]);


        DB::table('user_todos')->insert([
            'user_id'  => 5,
            'text'     => 'To do line 3333...',
            'priority' => 2,
            'task_id'  => 2,
            'completed'=> false,
        ]);


    }


}
