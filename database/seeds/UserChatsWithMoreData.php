<?php

use Illuminate\Database\Seeder;

class UserChatsWithMoreData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            DB::table('user_chats')->insert([
                'name'              => 'Let\'s discuss task Mastering Laravel/vue.js in this chat...',
                'description'       => 'Let\'s discuss task Mastering Laravel/vue.js in this chat... and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
                'creator_id'        => 1,
                'task_id'           => 1, #
                'status'            => 'A',
            ]);

            DB::table('user_chats')->insert([
                'name'              => 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion',
                'description'       => 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
                'creator_id'        => 2,
                'task_id'           => 2, //
                'status'            => 'A',
            ]);

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
