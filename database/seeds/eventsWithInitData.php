<?php

use Illuminate\Database\Seeder;

class eventsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([  // event_id = 1
            'name'      =>  'Members of "Mastering Laravel/vue.js" task would discuss how to start the task',
            'access'    =>  'P',  // P=>Private, U=>Public
            'at_time'   =>  '2018-02-08 17:30:00',
            'duration'  =>  80,
            'task_id'   =>  1,
            'description'      =>  'Members of "Mastering Laravel/vue.js" task would discuss how to start the task description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  1,
            'user_id'     =>  3,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  1,
            'user_id'     =>  10,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  1,
            'user_id'     =>  5,
        ]);

        DB::table('events')->insert([   // event_id = 2
            'name'      =>  'Members of "Mastering Laravel/vue.js" task would discuss the started project',
            'access'    =>  'P', // P=>Private, U=>Public
            'at_time'   =>  '2018-02-12 10:00:00',
            'duration'  =>  60,
            'task_id'   =>  1,
            'description'      =>  'Members of "Mastering Laravel/vue.js" task would discuss the started project description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  2,
            'user_id'     =>  3,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  2,
            'user_id'     =>  10,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  2,
            'user_id'     =>  5,
        ]);




        DB::table('events')->insert([   // event_id = 3
            'name'      =>  'Members of "Develop Tasks management site using Laravel/vue.js" task would discuss how to start the task',
            'access'    =>  'P', // P=>Private, U=>Public
            'at_time'   =>  '2018-02-10 17:50:00',
            'duration'  =>  30,
            'task_id'   =>  2,
            'description'      =>  'Members of "Develop Tasks management site using Laravel/vue.js" task would discuss how to start the task description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  3,
            'user_id'     =>  3,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  3,
            'user_id'     =>  10,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  3,
            'user_id'     =>  5,
        ]);


        DB::table('events')->insert([   // event_id = 4
            'name'      =>  'Members of "Develop Tasks management site using Laravel/vue.js" task would discuss the started project',
            'access'    =>  'P', // P=>Private, U=>Public
            'at_time'   =>  '2018-02-12 10:00:00',
            'duration'  =>  60,
            'task_id'   =>  2,
            'description'      =>  'Members of "Develop Tasks management site using Laravel/vue.js" task would discuss the started project description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  4,
            'user_id'     =>  3,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  4,
            'user_id'     =>  10,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  4,
            'user_id'     =>  5,
        ]);



        DB::table('events')->insert([   // event_id = 5
            'name'      =>  'Celebrate birthday of our boss',
            'access'    =>  'U', // P=>Private, U=>Public
            'at_time'   =>  '2018-02-15 17:00:00',
            'duration'  =>  240,
            'task_id'   =>  null,
            'description'      =>  'Celebrate birthday of our boss description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  1,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  2,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  3,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  4,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  5,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  6,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  7,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  8,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  9,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  10,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  11,
        ]);

        DB::table('events_users')->insert([
            'event_id'    =>  5,
            'user_id'     =>  12,
        ]);

    }
}
