<?php

use Illuminate\Database\Seeder;

class CategoriesAddMoreData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();


            DB::table('categories')->insert([
                'id'          => 8,
                'name'        => 'HTML-programming',
                'description' => 'HTML-programming description...',
                'parent_id'   => null,
            ]);

            DB::table('categories')->insert([
                'id'          => 9,
                'name'        => 'Bootstrap Development',
                'description' => 'Bootstrap Development description...',
                'parent_id'   => 8,
            ]);

            DB::table('categories')->insert([
                'id'          => 10,
                'name'        => 'vue-strap library testing',
                'description' => 'vue-strap library description ( https://wffranco.github.io/vue-strap/ )...',
                'parent_id'   => 9,
            ]);

            DB::table('categories')->insert([
                'id'          => 11,
                'name'        => 'Learning Bootstrap 4 Cookbook book',
                'description' => 'Learning Bootstrap 4 Cookbook book description ...',
                'parent_id'   => 9,
            ]);

            DB::table('categories')->insert([
                'id'          => 12,
                'name'        => 'Learning Bootstrap 4 Essential Training ',
                'description' => 'Learning Bootstrap 4 Essential Training description ...',
                'parent_id'   => 9,
            ]);




            DB::table('categories')->insert([
                'id'          => 13,
                'name'        => 'Responsive design with Skeleton',
                'description' => 'Responsive design with Skeleton description...',
                'parent_id'   => 8,
            ]);

            DB::table('categories')->insert([
                'id'          => 14,
                'name'        => 'Learn Skeleton at http://getskeleton.com/ site',
                'description' => 'Learn Skeleton at http://getskeleton.com/ site description...',
                'parent_id'   => 13,
            ]);

            DB::table('categories')->insert([
                'id'          => 15,
                'name'        => 'Graphical tasks',
                'description' => 'Graphical tasks description...',
                'parent_id'   => null,
            ]);

            DB::table('categories')->insert([
                'id'          => 16,
                'name'        => 'Learn Corel Draw',
                'description' => 'Learn Corel Draw description...',
                'parent_id'   => 15,
            ]);


            DB::table('categories')->insert([
                'id'          => 17,
                'name'        => 'Learn "Getting Started with Corel Painter Video"',
                'description' => 'Learn Corel Draw by "Getting Started with Corel Painter 2016 Training Video" description...',
                'parent_id'   => 16,
            ]);

            DB::table('categories')->insert([
                'id'          => 18,
                'name'        => 'Clipart for Corel Draw (12750)',
                'description' => 'Clipart for Corel Draw (12750) description...',
                'parent_id'   => 16,
            ]);



        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();


    }
}
