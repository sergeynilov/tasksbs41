<?php

use Illuminate\Database\Seeder;

class SettingsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'name' => 'items_per_page',
            'value' => 20,
        ]);
        DB::table('settings')->insert([
            'name' => 'site_name',
            'value' =>  'Loan import',
        ]);

    }
}
