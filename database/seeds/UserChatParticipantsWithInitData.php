<?php

use Illuminate\Database\Seeder;

class UserChatParticipantsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            DB::table('user_chat_participants')->insert([
                'user_id'           => 1,
                'user_chat_id'           => 1,
                'status'       => 'M',
            ]);


            DB::table('user_chat_participants')->insert([
                'user_id'           => 2,
                'user_chat_id'           => 1,
                'status'       => 'W',
            ]);


        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
