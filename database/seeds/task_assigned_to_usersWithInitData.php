<?php

use Illuminate\Database\Seeder;

class task_assigned_to_usersWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            DB::table('task_assigned_to_users')->insert([
                'task_id'        => 1,   // Mastering Laravel/vue.js
                'user_id' => 3,
                'user_task_type_id'=>1,
                'description'=>'You need to learn/control Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
            ]);

            DB::table('task_assigned_to_users')->insert([
                'task_id'        => 1,   // Mastering Laravel/vue.js
                'user_id' => 4,
                'user_task_type_id'=>2,
                'description'=>'You need to learn Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
            ]);


            DB::table('task_assigned_to_users')->insert([
                'task_id'        => 2,   // Develop Tasks management site using Laravel/vue.js
                'user_id' => 3,
                'user_task_type_id'=>1,
                'description'=>'You need to learn/control Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
            ]);

            DB::table('task_assigned_to_users')->insert([
                'task_id'        => 2,   // Develop Tasks management site using Laravel/vue.js
                'user_id' => 4,
                'user_task_type_id'=>2,
                'description'=>'You need to learn Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
            ]);


        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();

    }
}
