<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTasksLeaderId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('tasks', function (Blueprint $table) {
                $table->dropForeign('tasks_leader_id_foreign');
                $table->dropIndex('tasks_leader_id_status_index');
                $table->dropColumn('leader_id');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::table('tasks', function (Blueprint $table) {
                $table->integer('leader_id')->unsigned()->after('creator_id')->default(1);
                $table->foreign('leader_id')->references('id')->on('users')->onDelete('RESTRICT');
                $table->index(['leader_id', 'status'], 'tasks_leader_id_status_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
