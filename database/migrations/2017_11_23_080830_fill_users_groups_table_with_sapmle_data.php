<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Group;

class FillUsersGroupsTableWithSapmleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_groups', function (Blueprint $table) {
            DB::table('users_groups')->insert([
                'user_id' => 1,
                'group_id' => Group::getSimilarGroupByName('Admin')->id,
            ]);

            DB::table('users_groups')->insert([
                'user_id' => 2,
                'group_id' => Group::getSimilarGroupByName('Main Manager')->id,
            ]);

            DB::table('users_groups')->insert([
                'user_id' => 3,
                'group_id' => Group::getSimilarGroupByName('Manager')->id,
            ]);

            DB::table('users_groups')->insert([
                'user_id' => 4,
                'group_id' => Group::getSimilarGroupByName('Employee')->id,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_groups', function (Blueprint $table) {
            DB::table('users_groups')->delete();
        });
    }
}
