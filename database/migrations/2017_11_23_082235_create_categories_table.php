<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name',50)->unique();
            $table->string('description', 255);
            $table->string('image', 100)->nullable();

            $table->smallInteger('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('RESTRICT');

            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'categories_created_at_index');
        });
        Artisan::call('db:seed', array('--class' => 'CategoriesWithInitData'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function ($table) {
            $table->dropIndex('categories_created_at_index');
        });

        Schema::dropIfExists('categories');
    }
}
