<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaskTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_task_types', function (Blueprint $table) {
                $table->smallIncrements('id');
                $table->string('name',50)->unique();
                $table->mediumText('description');
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'user_task_types_created_at_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        Artisan::call('db:seed', array('--class' => 'UserTaskTypesWithInitData'));

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        try {
            DB::beginTransaction();

            Schema::table('user_task_types', function ($table) {
                $table->dropIndex('user_task_types_created_at_index');
            });
            Schema::dropIfExists('user_task_types');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
