<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTaskOperationsTableUserOperationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('task_operations', function (Blueprint $table) {
                $table->integer('user_operation_id')->unsigned()->after('status');
                $table->foreign('user_operation_id')->references('id')->on('users')->onDelete('RESTRICT');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('task_operations', function (Blueprint $table) {
                $table->dropForeign('task_operations_user_operation_id_foreign');
                $table->dropColumn('user_operation_id');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
