<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWeatherCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            Schema::create('user_weather_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->default(1);
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');
                $table->string('location', 255);



                $table->enum('location_type', ['C', 'I', 'G', 'Z'])->comment('C=>By city name, O=>By city id, G=By geographic coordinates, Z-By ZIP code');


                $table->timestamp('created_at')->useCurrent();

                $table->unique(['user_id', 'location'], 'user_weather_locations_user_id_location_unique');
                $table->index(['created_at'], 'task_operations_created_at_index');

            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        Artisan::call('db:seed', array('--class' => 'user_weather_citiesWithInitData'));
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::dropIfExists('user_weather_locations');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }
}
