<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserUserChatNewMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_user_chat_new_messages', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');

                $table->integer('user_chat_message_id')->unsigned();
                $table->foreign('user_chat_message_id')->references('id')->on('user_chat_messages');

                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'user_user_chat_new_messages_created_at_index');
                $table->index(['user_id', 'user_chat_message_id'], 'user_user_chat_new_messages_user_id_user_chat_message_id');

            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_user_chat_new_messages');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();

    }
}
