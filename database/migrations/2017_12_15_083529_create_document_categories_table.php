<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('document_categories', function (Blueprint $table) {
                $table->smallIncrements('id');
                $table->string('name', 255)->unique();
                $table->enum('type', ['D', 'C', 'P', 'M', 'T'])->comment(' D=>Task Document, C=>Chat Document, P=>Profile Document, M=>Profile Main Image, T=>Profile Thumbnail Image');
                $table->mediumText('description');
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'document_categories_created_at_index');
                $table->index(['type','name'], 'document_categories_name_at_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        Artisan::call('db:seed', array('--class' => 'DocumentCategoriesWithInitData'));

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('document_categories');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
