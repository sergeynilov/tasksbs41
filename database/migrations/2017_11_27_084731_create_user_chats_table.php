<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_chats', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 255);
                $table->mediumText('description');

                $table->integer('creator_id')->unsigned()->nullable();
                $table->foreign('creator_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->enum('status', ['A', 'C'])->comment(' A=>Active, C=>Closed');

                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'user_chats_created_at_index');
                $table->index(['creator_id', 'status', 'name'], 'user_chats_creator_id_status_name_index');
            });
            Artisan::call('db:seed', array('--class' => 'UserChatsWithInitData'));
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            
            Schema::dropIfExists('user_chats');
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
