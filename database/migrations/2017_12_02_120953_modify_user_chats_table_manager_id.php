<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatsTableManagerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            DB::beginTransaction();

            Schema::table('user_chats', function (Blueprint $table) {
                $table->integer('manager_id')->unsigned()->default(1);
                $table->foreign('manager_id')->references('id')->on('users')->onDelete('RESTRICT');
                $table->index(['manager_id', 'status'], 'user_chats_manager_id_status_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        try {
            DB::beginTransaction();

            Schema::table('user_chats', function (Blueprint $table) {
                $table->dropForeign('user_chats_manager_id_foreign');

                $table->dropIndex('user_chats_manager_id_status_index');
                $table->dropColumn('manager_id');

            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
