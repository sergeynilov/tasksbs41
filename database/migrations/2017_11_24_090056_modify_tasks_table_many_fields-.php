<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTasksTableManyFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            DB::beginTransaction();

            Schema::create('tasks', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();

                $table->integer('creator_id')->unsigned();
                $table->foreign('creator_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->smallInteger('category_id')->unsigned();
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('RESTRICT');

                $table->enum('priority', ['0', '1', '3', '4', '5'])->default("0")->comment('  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ');
                $table->enum('status', ['D', 'A', 'C', 'P', 'K', 'O'])->comment('D => Draft, A=>Assigning, C => Cancelled, P => Processing, K=> Checking, O=> Completed');

                $table->date('date_start');
                $table->date('date_end');

                $table->enum('needs_reports', ['0', '1', '3', '4', '5'])->default("0")->comment('0=>No, 1=>Hourly, 2=>Twice a day,3=>Daily,4=>Twice a week,5=>Weekly');

                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();

                $table->index(['status', 'date_start', 'date_end'], 'tasks_status_date_start_date_end_index');
                $table->index(['priority', 'category_id', 'date_end'], 'tasks_priority_category_id_date_end_index');
                $table->index(['created_at'], 'tasks_created_at_index');
                
            });

            Artisan::call('db:seed', array('--class' => 'TasksWithInitData'));

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::table('tasks', function ($table) {
                $table->dropIndex('tasks_status_date_start_date_end_index');
                $table->dropIndex('tasks_priority_category_id_date_end_index');
                $table->dropIndex('tasks_created_at_index');
            });
            Schema::dropIfExists('tasks');

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }
}
