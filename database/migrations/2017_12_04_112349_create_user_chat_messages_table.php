<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_chat_messages', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->integer('user_chat_id')->unsigned();
                $table->foreign('user_chat_id')->references('id')->on('user_chats')->onDelete('RESTRICT');

                /* CREATE TABLE `tsk_user_chat_message_documents` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_chat_message_id` INT(10) UNSIGNED NOT NULL,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`filename` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`extension` VARCHAR(10) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`info` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`document_category_id` SMALLINT(5) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `user_chat_message_documents_1_unique` (`user_chat_message_id`, `user_id`, `filename`),
	INDEX `user_chat_message_documents_user_id_foreign` (`user_id`),
	INDEX `user_chat_message_documents_document_category_id_foreign` (`document_category_id`),
	INDEX `user_chat_message_documents_2` (`user_chat_message_id`, `user_id`, `extension`),
	INDEX `user_chat_message_documents_created_at_index` (`created_at`),
	CONSTRAINT `user_chat_message_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
	CONSTRAINT `user_chat_message_documents_user_chat_message_id_foreign` FOREIGN KEY (`user_chat_message_id`) REFERENCES `tsk_user_chat_messages` (`id`),
	CONSTRAINT `user_chat_message_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`)
) */

                $table->boolean('is_top')->default(false);
                $table->mediumText('text');

                $table->timestamp('created_at')->useCurrent();

                $table->index(['user_id', 'user_chat_id'], 'user_chat_messages_user_id_user_chat_id_index');
                $table->index(['user_chat_id', 'is_top'], 'user_chat_messages_user_chat_id_is_top_index');
                $table->index(['created_at'], 'user_chat_messages_created_at_index');
            });
            Artisan::call('db:seed', array('--class' => 'UserChatMessagesWithInitData'));

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_chat_messages');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
