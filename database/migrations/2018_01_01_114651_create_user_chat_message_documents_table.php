<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatMessageDocumentsTable_1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            DB::beginTransaction();

            Schema::create('user_chat_message_documents', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_chat_message_id')->unsigned();
                $table->foreign('user_chat_message_id')->references('id')->on('user_chat_messages')->onDelete('RESTRICT');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->string('filename', 255);
                $table->string('extension', 10);
                $table->string('info', 255)->nullable();

                $table->smallInteger('document_category_id')->unsigned();
                $table->foreign('document_category_id')->references('id')->on('document_categories')->onDelete('RESTRICT');


                $table->timestamp('created_at')->useCurrent();

                $table->unique(['user_chat_message_id', 'user_id', 'filename'], 'user_chat_message_documents_1_unique');
                $table->index(['user_chat_message_id', 'user_id', 'extension'], 'user_chat_message_documents_2');
                $table->index(['created_at'], 'user_chat_message_documents_created_at_index');
        });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_chat_message_documents');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
