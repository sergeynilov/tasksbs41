<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatsLastVisitedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_chats_last_visited', function (Blueprint $table) {

                $table->bigIncrements('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->integer('user_chat_id')->unsigned();
                $table->foreign('user_chat_id')->references('id')->on('user_chats')->onDelete('RESTRICT');
                $table->timestamp('visited_at');

                $table->unique(['user_id', 'user_chat_id'], 'user_chats_last_visited_user_id_user_chat_id_unique');
                $table->index(['visited_at'], 'user_chats_last_visited_visited_at_index');

            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_chats_last_visited');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
    }
}
