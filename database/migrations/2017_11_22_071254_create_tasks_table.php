<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('address');
            $table->string('website')->nullable();
            $table->string('email');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->index(['created_at'], 'tasks_created_at_index');

        });

        DB::table('tasks')->insert([
            'name' => 'Kill\'em all',
            'address' => 'address 1',
            'email' => 'email1@site.com',
            'website' => 'website1.com',
        ]);
        DB::table('tasks')->insert([
            'name' => 'Let it be',
            'address' => 'address 2',
            'email' => 'email2@site.com',
            'website' => 'website2.com',
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function ($table) {
            $table->dropIndex('tasks_created_at_index');
        });
        Schema::dropIfExists('tasks');
    }
}
