<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            DB::beginTransaction();

            Schema::create('user_profile_documents', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->string('filename', 255);
                $table->string('extension', 10);

                $table->smallInteger('document_category_id')->unsigned();
                $table->foreign('document_category_id')->references('id')->on('document_categories')->onDelete('RESTRICT');

                $table->enum('public_access', ['P', 'E'])->default("E")->comment('  P-Public, E-Personal');

                $table->string('info', 255)->nullable();
                $table->timestamp('created_at')->useCurrent();

                $table->unique(['user_id', 'filename'], 'user_profile_documents_user_id_filename_unique');
                $table->index(['user_id', 'public_access'], 'user_profile_documents_user_id_public_access_unique');
                $table->index(['user_id', 'extension'], 'user_profile_documents_user_id_extension_unique');
                $table->index(['created_at'], 'user_profile_documents_created_at_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::dropIfExists('user_profile_documents');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
