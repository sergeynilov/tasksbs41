<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskAssignedToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('task_assigned_to_users', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('task_id')->unsigned();
                $table->foreign('task_id')->references('id')->on('tasks')->onDelete('RESTRICT');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->smallInteger('user_task_type_id')->unsigned();
                $table->foreign('user_task_type_id')->references('id')->on('user_task_types')->onDelete('RESTRICT');

                $table->mediumText('description');

                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();

                $table->unique(['user_id', 'task_id'], 'task_assigned_to_users_user_id_task_id_unique');
                $table->index(['created_at'], 'task_assigned_to_users_created_at_index');

            });
            
            Artisan::call('db:seed', array('--class' => 'task_assigned_to_usersWithInitData'));

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('task_assigned_to_users');

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
