<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\UserChatMessage;

use Database\Factories\UserChatMessageFactory;

class AddFakerDataUserChatMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/* https://scotch.io/tutorials/generate-dummy-laravel-data-with-model-factories
https://laravel.com/docs/5.5/database-testing

https://github.com/fzaninotto/Faker */
        Schema::table('user_chat_messages', function (Blueprint $table) {

            try {
                DB::beginTransaction();
                DB::update( " UPDATE ".DB::getTablePrefix()."users SET first_name='Firstname1', last_name='Lastname1', phone='phone # 1', website='firstname_website_1',  ".
                " contract_start='2017-02-23', contract_end= '2018-09-23',	updated_at= '2018-02-28 08:49:32' where id= 1");


                DB::update( " UPDATE ".DB::getTablePrefix()."users SET first_name='Firstname2', last_name='Lastname2', phone='phone # 2', website='firstname_website_2',  ".
                " contract_start='2017-02-23', contract_end= '2018-09-23',	updated_at= '2018-02-28 08:49:32' where id= 2");


                DB::update( " UPDATE ".DB::getTablePrefix()."users SET first_name='Firstname3', last_name='Lastname3', phone='phone # 3', website='firstname_website_3',  ".
                " contract_start='2017-02-23', contract_end= '2018-09-23',	updated_at= '2018-02-28 08:49:32' where id= 3");


                DB::update( " UPDATE ".DB::getTablePrefix()."users SET first_name='Firstname4', last_name='Lastname4', phone='phone # 4', website='firstname_website_4',  ".
                " contract_start='2017-02-23', contract_end= '2018-09-23',	updated_at= '2018-02-28 08:49:32' where id= 4");

                $users = factory(App\User::class, 2)->create();
                foreach( $users as $next_key=>$nextUser ) {
                    $userChatMessagesList= factory( App\UserChatMessage::class, 5 )->make(); // create UserChatMessages list without saving it - saved would be later
                    foreach ($userChatMessagesList as $nextUserChatMessage) {
                        $nextUser->userChatMessages()->save($nextUserChatMessage);
                    }
                }

            } catch (Exception $e) {

                DB::rollBack();
                throw $e;
            }

            DB::commit();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('user_chat_messages', function (Blueprint $table) {

            try {
                DB::beginTransaction();

                DB::update( " DELETE FROM ".DB::getTablePrefix()."user_chat_messages where id >= 10 ");
                DB::update( " DELETE FROM ".DB::getTablePrefix()."users where id >= 10 ");

            } catch (Exception $e) {

                DB::rollBack();
                throw $e;
            }

            DB::commit();

        });

    }
}
