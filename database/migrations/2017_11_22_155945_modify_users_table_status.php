<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Traits\funcsTrait;

class ModifyUsersTableStatus extends Migration
{
    use funcsTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login', 50)->unique();
            $table->enum('status', ['N', 'A', 'I'])->default("N")->comment(' N => New(Waiting activation), A=>Active, I=>Inactive');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->index(['status', 'login'], 'users_status_login_index');
        });
        DB::table('users')->insert([
            'login' => 'admin',
            'name' => 'JonGlads',
            'email' => 'admin@site.com',
            'status' => 'A',
            'password' => bcrypt('111111'),
        ]);
        DB::table('users')->insert([
            'login' => 'rod_bodrick',
            'name' => 'RodBodrick',
            'email' => 'rod_bodrick@site.com',
            'status' => 'A',
            'password' => bcrypt('111111'),
        ]);

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('users_status_login_index');
            $table->dropColumn('login');
            $table->dropColumn('status');
        });
    }
}
