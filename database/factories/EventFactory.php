<?php

use App\Event;
use App\Task;
use App\library\ListingReturnData;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Event::class, function ($faker, $parentUser) {
    $eventAccessValueArray= array_keys(Event::getEventAccessValueArray(false));
    $tasksList = Task::getTasksList(ListingReturnData::LISTING, []);
    $tasksArray= [null];
    foreach( $tasksList as $nextTask ) {
        $tasksArray[]= $nextTask->id;
    }

    return [

        'name' => 'event ## : ' . $faker->sentence( /*$nbWords =*/ 6, /*$variableNbWords =*/ true),
        'access' => $faker->randomElement( $eventAccessValueArray ),
        'task_id' => $faker->randomElement( $tasksArray ),
        'at_time' => $faker->dateTimeThisMonth('now', Config::get('app.timezone'))->format('Y-m-d H:i:s'),
        'duration' => $faker->biasedNumberBetween(/*$min =*/ 10, /*$max = */ 240 ) ,
        'description' => 'description : ' . $faker->text( /*$maxNbChars = */200)  ,

    ];
});
