<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    $contract_start= $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null);
    $contract_end= $faker->dateTimeBetween($startDate = 'now', $endDate = ' 2 years', $timezone = null);

    $first_name = $faker->firstName;
    $last_name = $faker->lastName;
    $username= $first_name.' '.$last_name;
    return [
        'name' => $username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'status' => 'A',
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone' => $faker->tollFreePhoneNumber,
        'website' => str_slug($username).'.com',
        'contract_start' => $contract_start->format('Y-m-d H:i:s'),
        'contract_end' => $contract_end->format('Y-m-d H:i:s'),
    ];
});