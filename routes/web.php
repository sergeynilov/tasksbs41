<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});


Auth::routes();               // http://127.0.0.1:8000/admin/dashboard#/

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'DashboardController@logout');
Route::get('auth_register', 'Auth\RegisterController@listing')->name('auth_register');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/get_settings_value', 'DashboardController@get_settings_value');
Route::get('admin/get_logged_user_info', 'DashboardController@get_logged_user_info');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('events_dictionaries/{event_id}', 'EventsController@events_dictionaries')->name('events_dictionaries');

    Route::get('user_chats_create', 'UserChatsController@create')->name('user_chats_create');


    Route::get('get_user_view_data/{id}', 'DashboardController@get_user_view_data')->name('get_user_view_data');
    Route::get('user_profile', 'UserProfileController@index')->name('user_profile');
    Route::get('get_user_events/{id}', 'DashboardController@get_user_events')->name('get_user_events');
    Route::get('recalc_user_events/{id}', 'DashboardController@recalc_user_events')->name('recalc_user_events');
    Route::get('user_events_by_day_user_id/{date}/{user_id}', 'DashboardController@user_events_by_day_user_id')->name('user_events_by_day_user_id');


    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
//    Route::get('app_description', 'DashboardController@app_description')->name('app_description');
    Route::post('get_source_file', 'DashboardController@get_source_file')->name('get_source_file');
    Route::get('get_system_info', 'DashboardController@get_system_info')->name('get_system_info');

    Route::get('test', 'DashboardController@test')->name('test');
    Route::get('dashboard_profile_data', 'DashboardController@dashboard_profile_data')->name('dashboard_profile_data');
    Route::get('refresh_user_profile_quick_info', 'DashboardController@refresh_user_profile_quick_info')->name('refresh_user_profile_quick_info');
    Route::get('get_assigning_tasks_details', 'DashboardController@get_assigning_tasks_details')->name('get_assigning_tasks_details');


    Route::get('get_weather_info_by_location/{location}', 'DashboardController@get_weather_info_by_location')->name('get_weather_info_by_location');
    Route::get('user_weather_location_load', 'DashboardController@user_weather_location_load')->name('user_weather_location_load');
    Route::post('user_weather_location_store', 'DashboardController@user_weather_location_store')->name('user_weather_location_store');
    Route::patch('user_weather_location_update/{user_weather_location_id}', 'DashboardController@user_weather_location_update')->name('user_weather_location_update');
    Route::delete('user_weather_location_destroy/{user_weather_location_id}', 'DashboardController@user_weather_location_destroy')->name('user_weather_location_destroy');


    Route::post('user_todo_store', 'DashboardController@user_todo_store')->name('user_todo_store');
    Route::get('user_todos_load', 'DashboardController@user_todos_load')->name('user_todos_load');
    Route::post('user_todo_store', 'DashboardController@user_todo_store')->name('user_todo_store');
    Route::patch('user_todo_update/{user_todo_id}', 'DashboardController@user_todo_update')->name('user_todo_update');
    Route::delete('user_todo_destroy/{user_todo_id}', 'DashboardController@user_todo_destroy')->name('user_todo_destroy');

    Route::get('user_todos_dictionaries', 'DashboardController@user_todos_dictionaries')->name('user_todos_dictionaries');

    // src_file + "&file_type
//    Route::get('get_source_file/{filename}/{file_type}', 'DashboardController@get_source_file')->name('SourceViewer');
    // http://local-tasks.com/admin/dashboard#/admin/source_viewer/resources%2Fassets%2Fjs%2Fcomponents%2Flib%2FStatusLine.vue/vue
    //                     window.API_VERSION_LINK + '/get_source_file?src_file=' + this.src_file + "&file_type=" + this.file_type ).then((response) => {

//    Route::resource('dashboard', 'DashboardController', ['except' => ['create', 'edit']])->middleware('WorkTextString');

    // API
    /*

    You cannot have two dynamic routes in the same syntax for example

Route::get('tasks/{filter}'...
Route::get('tasks/{id}'...
You must change either one of them, since tasks/{id} is resource route I suggest to change the filter route bit like

Route::get('tasks/filter/{filter}', 'TasksController@tasksFilter');
Route::resource('tasks', 'TasksController', ['except' => ['create', 'edit']])->middleware('WorkTextString');

     * */
    Route::get('tasks/filter/{filter}', 'TasksController@tasksFilter')->name('TasksFilter');
    Route::resource('tasks', 'TasksController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::resource('events', 'EventsController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::get('tasks_dictionaries/{id}', 'TasksController@dictionaries')->name('tasks_dictionaries');

    Route::delete('category_destroy/{category_id}', 'TasksController@category_destroy')->name('category_destroy');


//    Route::post('accept_task_to_processing/{task_id}','TasksController@accept_task_to_processing')->middleware('WorkTextString');
    Route::post('accept_task_to_cancelled/{task_assigned_to_user_id}','TasksController@accept_task_to_cancelled')->middleware('WorkTextString');
    Route::post('accept_task_to_checking/{task_assigned_to_user_id}','TasksController@accept_task_to_checking')->middleware('WorkTextString');
    Route::post('accept_task_to_processing/{task_assigned_to_user_id}','TasksController@accept_task_to_processing')->middleware('WorkTextString');
    Route::post('accept_task_to_complete/{task_assigned_to_user_id}','TasksController@accept_task_to_complete')->middleware('WorkTextString');
    Route::get('get_task_assigned_to_user_info/{task_assigned_to_user_id}','TasksController@get_task_assigned_to_user_info')->middleware('WorkTextString');
    Route::post('load_task_operations_history/{request_type}/{filter_id}','TasksController@load_task_operations_history')->middleware('WorkTextString');
    Route::post('load_task_status_changes_history/{request_type}/{filter_id}','TasksController@load_task_status_changes_history')->middleware('WorkTextString');
    Route::get('categories_dictionaries/{category_id}', 'TasksController@categories_dictionaries')->name('categories_dictionaries');
    Route::post('categories', 'TasksController@category_store')->name('category_store');
    Route::get('categories/{category_id}', 'TasksController@category_show')->name('category_show');
    Route::patch('categories/{category_id}', 'TasksController@category_update')->name('category_update');

    Route::post('change_task_status/{task_id}','TasksController@change_task_status')->middleware('WorkTextString');


    Route::resource('user_task_types', 'UserTaskTypesController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::resource('document_categories', 'DocumentCategoriesController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::get('document_categories_dictionaries', 'DocumentCategoriesController@dictionaries')->name('document_categories_dictionaries');

    // http://local-tasks.com/admin/document_categories?page=1&order_by=name&order_direction=asc

    // app/Http/Controllers/UserProfilesController.php

    Route::get('user_profiles', 'UserProfileController@show');
    Route::get('load_user_skills', 'UserProfileController@load_user_skills');
    Route::get('load_task_assigned_to_users_lists', 'UserProfileController@load_task_assigned_to_users_lists');
    Route::get('load_user_profile_documents', 'UserProfileController@load_user_profile_documents');
    Route::post('user_profiles', 'UserProfileController@update')->middleware('WorkTextString');
    Route::post('user_profiles_documents_upload', 'UserProfileController@user_profiles_documents_upload')->middleware('WorkTextString');
    Route::post('user_skills_update', 'UserProfileController@user_skills_update')->middleware('WorkTextString');
    Route::post('add_new_user_skill', 'UserProfileController@add_new_user_skill')->middleware('WorkTextString');

    Route::get('load_user_chat_message_documents', 'UserChatsController@load_user_chat_message_documents');
    Route::post('user_chat_message_documents_upload', 'UserChatsController@user_chat_message_documents_upload')->middleware('WorkTextString');
    Route::delete('user_chat_message_document_destroy/{id}', 'UserChatsController@user_chat_message_document_destroy');
    Route::patch('update_user_chat_message_document/{id}', 'UserChatsController@update_user_chat_message_document')->middleware('WorkTextString');



    Route::resource('user_chats', 'UserChatsController', ['except' => ['create', 'edit', 'run']])->middleware('WorkTextString');
    Route::get('user_chats_dictionaries/{user_chat_id}', 'UserChatsController@user_chats_dictionaries')->name('user_chats_dictionaries');


    Route::get('user_chat_run/{id}', 'UserChatsController@run');
    Route::get('load_user_chat_messages_list/{id}', 'UserChatsController@load_user_chat_messages_list');
    Route::delete('user_chat_message_destroy/{id}', 'UserChatsController@user_chat_message_destroy');
    Route::post('send_user_chat_new_message','UserChatsController@send_user_chat_new_message')->middleware('WorkTextString');
    Route::post('load_related_user_chats_list/{request_type}/{filter_id}','UserChatsController@load_related_user_chats_list');
    Route::get('load_related_events_list/{request_type}/{filter_id}','EventsController@load_related_events_list');

    Route::patch('update_user_chat_message/{id}', 'UserChatsController@update_user_chat_message')->middleware('WorkTextString');



    Route::delete('user_profile_document_destroy/{id}', 'UserProfileController@user_profile_document_destroy');


    /*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/chat', function () {
    return view('chat');
})->middleware('auth');

Route::get('/messages', function () {
    return App\Message::with('user')->get();
})->middleware('auth');

Route::post('/messages', function () {
    // Store the new message
    $user = Auth::user();

    $message = $user->messages()->create([
        'message' => request()->get('message')
    ]);

    // Announce that a new message has been posted
    broadcast(new MessagePosted($message, $user))->toOthers();

    return ['status' => 'OK'];
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index');
 */
    /*                                     <li><a href="{{ route('admin.user_chats.index') }}">My chats</a></li>
                                    <li><a href="{{ route('admin.user_chats_last.index') }}">My last chat</a></li>
                                    <li><a href="{{ route('admin.user_chats_new.index') }}">New chat</a></li>
 */
});

