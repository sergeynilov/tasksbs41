let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');


let current_dashboard_template= 'TasksBs4'
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/' + current_dashboard_template + '/app.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_lg.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_md.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_sm.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_xs_320.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_xs_480.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/assets/sass/' + current_dashboard_template + '/style_xs_600.scss', 'public/css/' + current_dashboard_template);

mix.copy('node_modules/font-awesome/fonts', 'public/fonts');