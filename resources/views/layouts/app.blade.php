<?php $current_dashboard_template= 'TasksBs4' ?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}
    <title id="app_title">@if( !empty($site_name) ){{ $site_name }}@endif</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/'.$current_dashboard_template.'/app.css') }}" rel="stylesheet">

    {{--iPhone portrait 320 x 480 --}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_320.css') }}" media="only screen and (min-width: 320px) and (max-width: 479px) " />

    {{--iPhone landscape 480 x 320--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_480.css') }}" media="only screen and (min-width: 480px)  and (max-width: 599px) " />

    {{--Kindle portrait 600 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_600.css') }}" media="only screen and (min-width: 600px)  and (max-width: 767px) " />

    {{--iPad portrait 768 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_sm.css') }}" media="only screen and (min-width: 768px)  and (max-width: 1023px) " />

    {{--iPad landscape 1024 x 768--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_md.css') }}" media="only screen and (min-width: 1024px) and (max-width: 1279px) " />

    {{--Macbook 1280 x 800--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_lg.css') }}" media="only screen and (min-width: 1280px)" />


    
    {{--<link href="{{ asset('css/materia/bootstrap.min.css') }}" rel="stylesheet">          <!-- materia 2 -->--}}
    {{--<link href="{{ asset('css/cosmo/bootstrap.min.css') }}" rel="stylesheet">        <!-- cosmo 2 -->--}}
    {{--<link href="{{ asset('css/darkly/bootstrap.min.css') }}" rel="stylesheet">   <!-- Darkly +4 -->--}}
    {{--<link href="{{ asset('css/cyborg/bootstrap.min.css') }}" rel="stylesheet">   <!-- cyborg 4 -->--}}
    {{--<link href="{{ asset('css/flatly/bootstrap.min.css') }}" rel="stylesheet">   <!-- flatly 3 -->--}}
    {{--<link href="{{ asset('css/journal/bootstrap.min.css') }}" rel="stylesheet">   <!-- journal 3 -->--}}
    {{--<link href="{{ asset('css/litera/bootstrap.min.css') }}" rel="stylesheet">   <!-- litera 3+ -->--}}
</head>
<body>
<div id="app">

    <app-header ></app-header>

    <main >
        @yield('content')
    </main>

    <status-line></status-line>

</div>

@include('layouts.footer')


</body>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

</html>
