
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

let current_dashboard_template= 'TasksBs4';

require('./bootstrap');

require('jquery');
require('jquery-confirm');
import 'jquery-confirm/css/jquery-confirm.css' // file:///_wwwroot/lar/TasksBootstrap41/node_modules/jquery-confirm/css/jquery-confirm.css


window.Vue = require('vue');

import Vue from 'vue';

import Element from 'element-ui'


import ElementUI from 'element-ui'; // https://github.com/ElemeFE/element
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale })

Vue.use(require('vue-moment'));

// debugger
import VueRouter from 'vue-router';


// import vSelect from 'vue-select' // https://github.com/sagalbot/vue-select
// Vue.component('v-select', vSelect)

// jquery-confirm
// import jqueryConfirm from 'jquery-confirm';


window.Vue.use(VueRouter);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component( 'app-header',  require( './components/lib/AppHeader.vue') );
Vue.component( 'status-line',  require( './components/lib/StatusLine.vue') );


import NotFound from './components/NotFound.vue'; // resources/assets/js/components/NotFound.vue
import AuthRegister from './components/Auth/Register.vue';
// import AuthLogin from './components/'+current_dashboard_template+'Auth/Login.vue';
import AuthLogin from './components/Auth/Login.vue';


import TasksIndex from './components/tasks/TasksIndex.vue';
import TasksFilter from './components/tasks/TasksIndex.vue';
import CategoryEdit from './components/tasks/CategoryEdit.vue';
import TasksEdit from './components/tasks/TasksEdit.vue';
import TaskOperation from './components/tasks/TaskOperation.vue';
import WeatherViewer from './components/lib/WeatherViewer.vue';
import UserTodos from './components/lib/UserTodos.vue';

import UserTaskTypesIndex from './components/user_task_types/UserTaskTypesIndex.vue';
import UserTaskTypesEdit from './components/user_task_types/UserTaskTypesEdit.vue';

import EventsIndex from './components/events/EventsIndex.vue';
import EventsEdit from './components/events/EventsEdit.vue';

import DocumentCategoriesIndex from './components/document_categories/DocumentCategoriesIndex.vue';
import DocumentCategoryEdit from './components/document_categories/DocumentCategoryEdit.vue';

import DashboardIndex from './components/dashboard/DashboardIndex.vue';
import TestIndex from './components/test/Test.vue';



import AppDescription from './components/dashboard/AppDescription.vue';

import UserChatsIndex from './components/user_chats/UserChatsIndex.vue';
import UserChatsEdit from './components/user_chats/UserChatsEdit.vue';

import UserChatsRun from './components/user_chats/UserChatsRun.vue';

// import {DatePicker} from 'uiv'
// import my_table from 'vue-simple-table';
// Vue.component('my-table', my_table)

// Vue.directive('focus', { inserted(el) { el.focus() } })
// Vue.directive('selected', { inserted(el) { el.select() } })
//


import appMixin from './appMixin';



import UserProfilesEdit from './components/user_profiles/UserProfilesEdit.vue';
import UserProfileView from './components/user_profiles/UserProfileView.vue';



const routes = [
    {
        path: '/',
        components: {
            // commonElements:CommonElements,
            notFound: NotFound,
            auth_register: AuthRegister,                      /* router-view name in .vue file : imported reference to .vue file */
            tasksIndex: TasksIndex,
            tasksFilter: TasksFilter,
            userTaskTypesIndex: UserTaskTypesIndex,


            eventsIndex: EventsIndex,


            documentCategoriesIndex: DocumentCategoriesIndex,
            userChatsIndex: UserChatsIndex,
            dashboardIndex: DashboardIndex,
            testIndex: TestIndex,
            appDescriptionIndex: AppDescription,

            userProfilesEdit: UserProfilesEdit,
            userProfileView: UserProfileView,
        }
    },
    {path: 'auth_register', component: AuthRegister, name: 'auth_register'},
    {path: '/auth/login', component: AuthLogin, name: 'AuthLogin'},
    {path: '/not-found/:invalid_url?', component: NotFound, name: 'notFound'},


    // {path: '/admin/tasks/create', component: TasksCreate, name: 'createTask'},

    {path: '/admin/tasks/category/:id', component: CategoryEdit, name: 'editCategory'},
    {path: '/admin/tasks/edit/:id', component: TasksEdit, name: 'editTask'},

    {path: '/admin/task_operation/:id', component: TaskOperation, name: 'taskOperation'},
    {path: '/admin/weather_viewer', component: WeatherViewer, name: 'weatherViewer'},
    {path: '/admin/user_todos', component: UserTodos, name: 'userTodos'},
    {path: '/admin/app_description', component: AppDescription, name: 'appDescription'},
    {path: '/admin/dashboard', component: DashboardIndex, name: 'dashboardIndex'}, // ?
    {path: '/admin/test', component: TestIndex, name: 'testIndex'}, //            testIndex: TestIndex,
    // {path: '/admin/tasks/filter/:filter', component: TasksFilter, name: 'tasksFilter'},
    {path: '/admin/tasks/filter/:filter', component: TasksFilter, name: 'tasksFilter'},
    {path: '/admin/tasks', component: TasksIndex, name: 'tasksIndex'},
    {path: '/admin/events', component: EventsIndex, name: 'eventsIndex'},
    // {path: '/admin/source_viewer/:src_file/:file_type', component: SourceViewer, name: 'sourceViewer'},
    // {path: '/admin/source_viewer/:src_file', component: SourceViewer, name: 'sourceViewer'},
    // {path: '/admin/source_viewer/:filename/:ext', component: SourceViewer, name: 'sourceViewer'},


    {path: '/admin/user_task_types', component: UserTaskTypesIndex, name: 'userTaskTypesIndex'},
    {path: '/admin/user_task_types/edit/:id', component: UserTaskTypesEdit, name: 'editUserTaskType'},
    // {path: '/admin/events/edit/:id', component: EventsEdit, name: 'editEvent'},
    {path: '/admin/events/edit/:id/:default_date?', component: EventsEdit, name: 'editEvent'},

    {path: '/admin/user_chats/:filter', component: UserChatsIndex, name: 'userChatsIndex'},
    {path: '/admin/user_chats/edit/:id', component: UserChatsEdit, name: 'editUserChat'},
    {path: '/admin/user_chat_run/:id', component: UserChatsRun, name: 'runUserChat'},


    // {path: '/admin/document_categories/create', component: DocumentCategoryCreate, name: 'createDocumentCategory'},
    // documentCategoriesIndex: DocumentCategoriesIndex,

    {path: '/admin/document_categories', component: DocumentCategoriesIndex, name: 'documentCategoriesIndex'},
    {path: '/admin/document_categories/edit/:id', component: DocumentCategoryEdit, name: 'editDocumentCategory'},

    {path: '/admin/user_profile', component: UserProfilesEdit, name: 'editUserProfiles'},
    {path: '/admin/user_profile_view/:id', component: UserProfileView, name: 'userProfileView'},
    //     {path: '/admin/task_operation/:id', component: TaskOperation, name: 'taskOperation'},

]

// alert("-02")   UserProfileEdit.vue
// alert( "routes::"+var_dump(routes) )
console.log("routes::")
console.log( routes )

const router = new VueRouter( {
    mode: 'hash', // default
    routes
})


router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next(  '/not-found/'+encodeURIComponent(to.path)  );
    } else {
        next();
    }
})


export const bus = new Vue();


new Vue({ router,

    data:{
        app_title: '',
        loggedUserProfile: {},
        loggedUserInGroups: {},
        refsArray:[]
    },

    mixins : [appMixin],

    methods: {


        initEchoServices() {
            console.log("!1initEchoServices::")

            Echo.channel('nsn_tasks')
                .listen('UserProfileModified', (e) => {
                    console.log('Inside of UserProfileModified::')
                    console.log(e)
                });


            Echo.private('nsn_tasks_chat')
                .listenForWhisper('typing', (e) => {
                    // alert( "listenForWhisper e::"+var_dump(e) )
                    console.log(" resources/assets/js/app.js : listenForWhisper::");
                    // console.log("e:::");
                    // console.log(e);
                    // console.log(e.logged_user_id);
                    // console.log(e.logged_user_background_color);
                    // console.log(e.logged_user_color);
                    //  console.log("e.user_chat_new_message:::");
                    //  console.log(e.user_chat_new_message);

                    // if (jQuery.trim(e.user_chat_new_message) != '') {
                    //     this.typing = 'typing...'
                    // }else{
                    //     this.typing = ''
                    // }

                    bus.$emit('userChatTypingEvent', {
                        'typing_username' : e.logged_username,
                        'typing_user_id' : e.logged_user_id,
                        'typing_user_background_color' : e.logged_user_background_color,
                        'typing_user_color' : e.logged_user_color,
                        'typing_text' : e.user_chat_new_message,
                        'user_chat_id' : e.user_chat_id,
                        'created_at': this.getNowDateTime()
                    });

                })

                .listen('ChatEvent', (e) => { // received triggered event from other user to refresh chat messages listing of current chart
                    // alert( "resources/assets/js/app.js ChatEvent  e::"+var_dump(e) )
                    console.log("??? resources/assets/js/app.js ChatEvent::");
                    console.log(e);

                    console.log("this.loggedUserProfile::");
                    console.log(this.loggedUserProfile);


                    // if ( e.user.id != this.loggedUserProfile['id'] ) { // fo not sent event to itself
                    bus.$emit('newUserChatAddedEvent', {
                        'user_chat_message_id': e.user_chat_message_id,
                        'user_chat_id': e.user_chat_id,
                        'author_user_id': e.user_id,
                        'author_name': e.author_name,
                        'message_type': e.message_type,
                        'text': e.text,
                        'is_top': e.is_top,
                        'created_at': this.getNowDateTime()
                    });
                    // } // if ( e.user.id != this.loggedUserProfile.id ) { // fo not sent event to itself

                })


            Echo.join(`nsn_tasks_chat`)
                .here((usersList) => {
                    var app_route_name= this.$route.name

                    console.log("!!here  app_route_name:::");
                    console.log(app_route_name)
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("here INSIDE of runUserChat ");
                    if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                        bus.$emit('setChartUsersListEvent', {'usersList': usersList});
                    } //                     if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                })
                .joining((user) => {
                    console.log(" joining this.$route::")
                    console.log( this.$route )

                    console.log("app_route_name::")
                    console.log( app_route_name )

                    var app_route_name= this.$route.name

                    // console.log(user.name);
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("joining INSIDE ");
                    if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                        this.showPopupMessage('User "' + user.name + '" joined this chat !', 'success');
                        bus.$emit('setChartUserJoiningEvent', {'user': user});
                    } // if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                })
                .leaving((user) => {
                    console.log("leaving app_route_name:::");
                    console.log(app_route_name)

                    var app_route_name= this.$route.name

                    // console.log(user.name);
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("joining INSIDE ");
                    if ( app_route_name == 'runUserChat' ) { // flash chat message only on chat page
                        this.showPopupMessage('User "' + user.name + '" leaving this chat !', 'warning');
                        bus.$emit('setChartUserLeavingEvent', {'user': user});
                    } // if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                });

        }, // initEchoServices() {

    }, // methods: {


    created() {
        // this.getSettingsValue('site_name','',window.API_VERSION_LINK, 'setSiteNameEvent', bus);

        bus.$on('UserProfileIsSetEvent', (username, site_name) => {
            this.app_title = site_name+" for "+username;
        })


        // bus.$on('moveToRefEvent', (ref_name) => {
        //     console.log("moveToRefEvent ref_name::")
        //     console.log( ref_name )
        //     console.log("moveToRefEvent this.$refs::")
        //     console.log( typeof this.$refs )
        //
        //
        //     var element = ''
        //     if ( ref_name == 'porto' ) {
        //
        //         console.log("+++this::")
        //         console.log( this )
        //
        //         console.log("+++!!!this.$refs::") // I show printscreen below
        //         console.log( this.$refs )
        //
        //         console.log("++++!!!ref_name::")
        //         console.log( ref_name )
        //         element= this.$refs[ref_name]
        //
        //     }
        //     console.log("?????? element::")
        //     console.log(element);
        //
        //     if ( typeof element!= "undefined") {
        //         var top = element.offsetTop;
        //         console.log("INSIDE top::")
        //         console.log( top )
        //
        //         window.scrollTo(0, top);
        //     }
        //
        //     // this.refsArray= this.$refs
        //
        // })

        // bus.$on('moveToRefEvent', (ref_name) => {
        //     console.log("moveToRefEvent ref_name::")
        //     console.log( ref_name )
        //     console.log("moveToRefEvent this.$refs::")
        //     console.log( this.$refs )
        //     // this.refsArray= this.$refs
        //
        // })
        /*
                bus.$on('setSiteNameEvent', (site_name) => {
                    // alert( "setSiteNameEvent site_name::"+site_name )
                    var dataArray= Array(site_name);
                    // alert( "this.loggedUserProfile::"+var_dump(this.loggedUserProfile) )
                    if ( typeof this.loggedUserProfile.first_name != "undefined" && typeof this.loggedUserProfile.last_name != "undefined" ) {
                        dataArray[dataArray.length-1] = this.loggedUserProfile.last_name +" "+ this.loggedUserProfile.first_name
                    }
                    var app_title= this.concatStrings (dataArray, '');
                    // alert( "app_title::"+var_dump(app_title) )
                    this.app_title = app_title;
                })
        */
    }, // created() {

    mounted() {

        // console.log(" mounted!! this.loggedUserProfile::");
        // console.log( typeof this.loggedUserProfile);
        // console.log(this.loggedUserProfile);
        //
        //
        // console.log(" mounted!! this.loggedUserProfile.id::");
        // console.log( typeof this.loggedUserProfile.id);
        // console.log(this.loggedUserProfile.id);
        //
        // alert( "window.logged_user_id::"+(window.logged_user_id) +"  window.logged_user_name::"+window.logged_user_name )

        console.log("this.$refs::")
        console.log( this.$refs )
        // this.refsArray= this.$refs

        if ( typeof window.logged_user_id == "undefined" || typeof window.logged_user_name == "undefined" ) return;// check if user is logged
        axios.get(window.API_VERSION_LINK + '/get_logged_user_info/' ).then((response) => {
            if ( typeof response.data.loggedUser == "object" ) {
                this.loggedUserProfile = response.data.loggedUser;
                this.loggedUserInGroups = response.data.loggedUserInGroups;
                bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name, this.loggedUserProfile, this.loggedUserInGroups );
                // alert( "LOGGED::"+(-9) )
                this.initEchoServices()
            }
        }).catch((error) => {
            this.showRunTimeError(error, this);
        });

    }, // mounted(){


} ).$mount('#app')  // new Vue({ router,



/*
const app = new Vue({
    el: '#app'
});
*/


var bootstrap4_enabled_text = ''
// var bootstrap4_enabled = (typeof $().emulateTransitionEnd == 'function');
// if (bootstrap4_enabled) {
//     bootstrap4_enabled_text = 'Bootstrap 4 enabled';
// }
var bootstrap_version = $.fn.tooltip.Constructor.VERSION
var jquery_version = $.fn.jquery;
if (typeof $.ui != "undefined") {
    var jquery_ui_version = $.ui.version;
}




Vue.filter('timeInAgoFormat', function (value) {
    if ( typeof value == "undefined" || typeof value == "object") return '';
    var formatted_val= moment(value).fromNow();
    // alert( "timeInAgoFormat value::"+(value) +"   formatted_val::"+formatted_val + "  :: " + ( typeof value ) )
    return formatted_val;
})

Vue.filter('Capitalize', function (value) {
    var ret= value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    return ret;
})

Vue.filter('toFixed', function (price, limit) {
    return price.toFixed(limit);
});

Vue.filter('toUSD', function (price) {
    return `$${price}`;
});

Vue.filter('json', function (value) {
    return JSON.stringify(value);
});

Vue.filter('pluck', function (objects, key) {
    return objects.map(function(object) {
        return object[key];
    });
});

Vue.filter('at', function (value, index) {
    return value[index];
});

Vue.filter('first', function (values) {
    if(Array.isArray(values)) {
        return values[0];
    }
    return values;
});

Vue.filter('last', function (values) {
    if(Array.isArray(values)) {
        return values[values.length - 1];
    }
    return values;
});

Vue.filter('without', function (values, exclude) { //Return a copy of the array without the given elements:
    return values.filter(function(element) {
        return !exclude.includes(element);
    });
});

Vue.filter('unique', function (values, unique) {
    return values.filter(function(element, index, self) {
        return index == self.indexOf(element);
    });
});

function var_dump (oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}

// alert( "Vue.version::"+( Vue.version) + ",  jquery_version::" + jquery_version + ",  bootstrap_version::"+bootstrap_version )