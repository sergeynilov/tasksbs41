export default {


//
//     Vue.filter('myOwnTime', function (value) {
//     return moment(value).fromNow();
//     // return moment(value).calendar();
// })

    computed: {

        // timeInAgoFormat (value) {
        //     return moment(value).fromNow();
        // // return moment(value).calendar();
        // },
        //
        // fruitsFiltered() {
        //     return this.fruits.filter((fruit) => {
        //         return fruit.match(this.filteredFruit)
        //     })
        // }
    }, // computed: {

    methods: {

        preg_quote( str ) {	// Quote regular expression characters
            return str.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },


        goToNamedRef(ref_name) {
            alert( "goToNamedRef ref_name::"+(ref_name) )
            console.log("Vue::")
            console.log( Vue )

            console.log("Vue.$refs::")
            console.log( Vue.$refs )

            console.log( this.$refs )
            console.log( typeof this.$refs )

            var element = ''
            if ( ref_name == 'porto' ) {

                console.log("+++this::")
                console.log( this )

                console.log("+++!!!this.$refs::") // I show printscreen below
                console.log( this.$refs )

                console.log("++++!!!ref_name::")
                console.log( ref_name )

            }
//                console.log("?????? element::")
//                console.log(element);

            if ( typeof element!= "undefined") {
                var top = element.offsetTop;
                console.log("INSIDE top::")
                console.log( top )

                window.scrollTo(0, top);
            }
        }, // goToNamedRef(ref_name) {

        addDaysToDate(add_days, current_date) {
            if ( typeof current_date == "undefined" ) {
                current_date = new Date();
            }
            current_date.setDate(current_date.getDate() + add_days);
            return current_date;
        },

        getPaginationParams : function(page, order_by, order_direction, is_question) {
            if ( typeof page != "number" ) page=1
            var ret= '';
            // debugger;
            if ( page > 0) {
                ret = ret + (is_question?'?':'&')+'page='+page
            }
            if ( typeof order_by != "undefined" && this.trim(order_by) != "") {
                ret = ret + (ret==''?'':'&')+'order_by='+order_by
            }

            if ( typeof order_direction != "undefined" && this.trim(order_direction) != "") {
                ret = ret + (ret==''?'':'&')+'order_direction='+order_direction
            }

            return ret;
        }, // getPaginationParams : function(page, order_by, order_direction, is_question) {


        setAppTitle : function(page_title_text, show_only_page_title, return_string) {
            // alert( "setAppTitle page_title_text::"+(page_title_text) )
            if ( typeof return_string == "undefined") return_string= false
            if ( typeof show_only_page_title == "undefined" || !show_only_page_title ) {
                if (typeof page_title_text != "undefined" && page_title_text != '' && window.SITE_NAME != "undefined" && window.SITE_NAME != '') {
                    // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                    if ( return_string ) return page_title_text + ' of ' + window.SITE_NAME;
                    if (document.getElementById("app_title")) {
                        // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  window.SITE_NAME::"+ window.SITE_NAME)
                        document.getElementById("app_title").innerHTML = page_title_text + ' of ' + window.SITE_NAME
                        return;
                    }
                }
                if (window.SITE_NAME != "undefined" && window.SITE_NAME != '') {
                    // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                    if ( return_string ) return window.SITE_NAME;
                    if (document.getElementById("app_title")) {
                        // alert( "+3+setAppTitle page_title_text::"+(page_title_text) +"  window.SITE_NAME::"+ window.SITE_NAME)
                        document.getElementById("app_title").innerHTML = window.SITE_NAME
                        return;
                    }
                }
            }
            if ( typeof page_title_text != "undefined" && page_title_text != '' ) {
                // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                if ( return_string ) return page_title_text;
                if ( document.getElementById("app_title") ) {
                    // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  window.SITE_NAME::"+ window.SITE_NAME)
                    document.getElementById("app_title").innerHTML = page_title_text
                    return;
                }
            }
        }, //Application name : Page Title

        inArray : function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        },

        effectiveDeviceWidth: function (param) {
            var viewport = {
                width: $(window).width(),
                height: $(window).height()
            };
            //alert( "viewport::"+var_dump(viewport) )
            if (typeof param != "undefined") {
                if (param.toLowerCase() == 'width') {
                    return viewport.width;
                }
                if (param.toLowerCase() == 'height') {
                    return viewport.height;
                }
            }
            return viewport;
            //var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
            //// iOS returns available pixels, Android returns pixels / pixel ratio
            //// http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
            //if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            //    deviceWidth = deviceWidth / window.devicePixelRatio;
            //}
            //return deviceWidth;
        },


        getBootstrapPlugins: function () {
            var ret_str = '';
            ret_str += "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "button:" + ( (typeof($.fn.button) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "carousel:" + ( (typeof($.fn.carousel) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "dropdown:" + ( (typeof($.fn.dropdown) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "modal:" + ( (typeof($.fn.modal) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "tooltip:" + ( (typeof($.fn.tooltip) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "popover:" + ( (typeof($.fn.popover) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "tab:" + ( (typeof($.fn.tab) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "affix:" + ( (typeof($.fn.affix) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "collapse:" + ( (typeof($.fn.collapse) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "scrollspy:" + ( (typeof($.fn.scrollspy) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "transition:" + ( (typeof($.fn.transition) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            //ret_str+= "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
            return ret_str;
        },


        getVueVersion: function () {
            return Vue.version;
        },


        formatNumberToHuman: function (number) {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },

        convertMinsToHrsMins: function (minutes) {
            if (minutes < 60) return (minutes + ' minutes');
            var h = Math.floor(minutes / 60);
            var m = minutes % 60;
            h = h < 10 ? h : h;
            m = m < 10 ? m : m;
            return h + ':' + m + ' hour(s)';
        },

        pluralize: function (n, single_str, multi_str) {
            return n === 1 ? single_str : multi_str
        },

        showFullUserName: function (username, first_name, last_name) {
            var ret_str = first_name + " " + last_name + " ( " + username + " ) ";
            return ret_str;
        },

        getHeaderIcon(icon) {
            if (typeof menuIconsArray[icon] != "undefined") return '<i class="' + menuIconsArray[icon] + '"></i>';
        },

        getActiveUserGroupLabel(loggedUserInGroupsArray) {
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = '';
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name != "undefined" && nextLoggedUserInGroup.status == 'A') {
                    ret = ret + nextLoggedUserInGroup.group_name + ', ';
                }
            });
            return ret;
        },

        checkUserGroupAccess(loggedUserInGroupsArray, group_name) {
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = false;
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name == group_name && nextLoggedUserInGroup.status == 'A') {
                    ret = true;
                }
            });
            return ret;
        },


        getIsPastColor(is_past) { //
            if (typeof settings_isPastColorsArray[is_past] != 'undefined') {
                return 'backgroundColor:' + settings_isPastColorsArray[is_past]
            }
            return '#fff'
        },

        getPriorityColor(priority) {
            // console.log("settings_priorityColorsArray::")
            // console.log( settings_priorityColorsArray )
            //
            // console.log("priority::")
            // console.log( priority )
            //
            if (typeof settings_priorityColorsArray[priority] != 'undefined') {
                return 'backgroundColor:' + settings_priorityColorsArray[priority]
            }
            return ''
        },

        dateToMySqlFormat(dat) {
            if (typeof dat != 'object') return dat;
            // alert( "dat::"+(dat) +  "   typeof  dat::"+(typeof dat) + "  dat::"+var_dump(dat) )

            var mm = dat.getMonth() + 1; // getMonth() is zero-based
            var dd = dat.getDate();

            return [dat.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
            ].join('-');
        },
        say(row) {
            alert('Open console and looking for response object!')
            console.log(row);
        },
        edit(value) {
            this.action.edit_value = value;
        },
        submit(res) {
            if (res.row[res.k] != this.action.edit_value) {
                let after_update = JSON.parse(JSON.stringify(res.row))
                this.$refs.my_table.set_row_undo(after_update, res.c)
                res.row[res.k] = this.action.edit_value
            }
            this.reset_edit()
        },
        reset_edit() {
            this.$refs.my_table.close()
        },
        set_checkbox(item) {
            this.action.checkbox = item
        },
        edit_row(row) {
            for (let k in row) {
                this.action.edit_row[k] = row[k]
            }
        },
        submit_row(row) {
            let after_update = JSON.parse(JSON.stringify(row))
            for (let key in this.action.edit_row) {
                row[key] = this.action.edit_row[key]
            }
            this.$refs.my_table.set_row_undo(after_update)
            this.$refs.my_table.close(true)
        },

        // timeInAgoFormat : function(value) {
        //     alert( "-2 timeInAgoFormat  value::"+(value) )
        //     return moment(value).fromNow();
        //     // return moment(value).calendar();
        // },
        //

        getSettingsValue: function (name, default_value, api_version_link, trigger_event_name, bus) {
            axios.get(api_version_link + '/get_settings_value?name=' + name)
                .then(function (response) {
                    // alert( "getSettingsValue response.data::"+(response.data) )
                    if (typeof trigger_event_name != "undefined" && typeof bus != "undefined") {
                        // alert( "INSIDE -1 trigger_event_name"+trigger_event_name )
                        bus.$emit(trigger_event_name, response.data.settings_value);
                    }
                    //                     bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name);

                    return response.data.settings_value;
                })
                .catch(function (error) {
                    // alert( "error::"+var_dump(error) )
//                     app.showRunTimeError(error, app);
                })
        },

        formatColor: function (rgb) {
            var isOk = /^#[0-9A-F]{6}$/i.test(rgb)
            // var isOk  = /^#[0-9A-F]{6}$/i.test('#aabbcc')
            // alert( "isOk::"+isOk )
            if (isOk) return rgb;

            // alert( typeof rgb )
            if (typeof rgb != "string" || this.trim(rgb) == "") return "";
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';


            var parts = rgba.substring(rgba.indexOf("(")).split(","),
                r = parseInt(this.trim(parts[0].substring(1)), 10),
                g = parseInt(this.trim(parts[1]), 10),
                b = parseInt(this.trim(parts[2]), 10),
                a = parseFloat(this.trim(parts[3].substring(0, parts[3].length - 1))).toFixed(2);

            return ('#' + r.toString(16) + g.toString(16) + b.toString(16) + (a * 255).toString(16).substring(0, 2));
            // // const rgba = "rgba(47, 154, 47, 1)"
            // const numbers = value.match(/[0-9]+/g)
            // numbers[3] = Math.round(numbers[3] * 255) //since the alpha is represented as 0-1 rather than 0-255
            // const hex = '#' + numbers.map(n => Number(n).toString(16)).join('')
            // return hex;
        },

        trim: function (str) {
            if ( typeof str != "string") return "";
            return str.replace(/^\s+|\s+$/gm, '');
        },

        concatStrings: function (dataArray, splitter) {
            var ret = '';
            // alert( "dataArray::"+(typeof dataArray)+"   "+var_dump(dataArray) )
            const l = dataArray.length;
            dataArray.map((next_string, index) => {
                // next_string = jQuery.trim(next_string);
                next_string = this.trim(next_string);                                                          1
                // alert( "next_string::"+(next_string) + (typeof next_string) +"  splitter::"+splitter )
                // if ( typeof next_string != "undefined" && typeof next_string != "string" ) {
                if (typeof next_string == "string") {
                    if (next_string) {
                        if (l === index + 1) {
                            ret = ret + next_string;
                        } else {
                            ret = ret + next_string + splitter;
                        }
                    } // if ( next_string ) {
                }
            });
            return ret;
        },

        showRunTimeError: function (error, app) {

            app.message = '';
            app.errorsList = [];
            // alert("::00000")

            if (( typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.message != "undefined" ) && ( typeof error.response.data.errors == "undefined" || error.response.data.errors.length == 0)) {
                // alert("::-1   "+ error.response.data.message )
                app.message = this.commonError(error.response.data.message);
            }
            // alert( "::-02" )
            if (typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.errors != "undefined" /*&& error.response.data.errors.length > 0*/) {
                // alert( "::-3" )
                app.errorsList = error.response.data.errors;
            }

            return app;
        },

        commonError: function (err_text) {
            return err_text;
        },

        showPopupMessage: function (message, type) {
            if (type == 'success') {         // https://www.npmjs.com/package/v-toaster
                this.$toaster.success(message)
            }
            if (type == 'info') {
                this.$toaster.info(message)
            }
            if (type == 'error') {
                this.$toaster.error(message)
            }
            if (type == 'warning') {
                this.$toaster.warning(message)
            }
        }, // showPopupMessage: function (message, type) {


        concatStr: function (str, max_str_length_in_listing) {
            if ( typeof settings_max_str_length_in_listing == "undefined" && typeof max_str_length_in_listing != "undefined" ) {
                var settings_max_str_length_in_listing = max_str_length_in_listing
            }

            if ( typeof str == "undefined" ) str= '';
            if (str.length > settings_max_str_length_in_listing) {
                return str.slice(0, settings_max_str_length_in_listing) + '...';
            }
            return str;
        },
        
        getNowDateTime: function () {
            return Date.now();
        },

        getNowTimestamp: function () {
            return Date.now() / 1000 | 0;
        },


        getFileSizeAsString: function (file_size) {
            if (parseInt(file_size) < 1024) {
                return file_size + 'b';
            }
            if (parseInt(file_size) < 1024 * 1024) {
                return Math.floor(file_size / 1024) + 'kb';
            }
            return Math.floor(file_size / (1024 * 1024)) + 'mb';
        },


        Capitalize: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },


        confirmMsg: function (question, confirm_function_code, title, icon) {

            if (typeof title == "undefined" || this.trim(title) == "") {
                title = 'Confirm!'
            }
            if (typeof icon == "undefined" || this.trim(icon) == "") {
                icon = 'fa fa-signal'
            }

            $.confirm({
                icon: icon,
                title: title,
                content: question,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                buttons: {
                    OK: {
                        text: 'OK',
                        btnClass: 'btn-blue',
                        keys: ['enter', 'a'],
                        isHidden: false,
                        isDisabled: false,
                        action: function(OKButton){
                            confirm_function_code()
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'ml-4 btn-grey',
                        keys: ['esc', 'e'],
                        isHidden: false,
                        isDisabled: false, 
                        action: function(cancelButton){
                        }
                    },
                }
            });

/*
            $.confirm({
                icon: icon,
                title: title,
                content: question,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                buttons: {
                    confirm: function () {
                        $.alert('Confirmed!');
                        confirm_function_code()
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    }
                }
            });
*/

        },

        func_setting_focus: function (focus_field) {
            $('#' + focus_field).focus();
        },

        alertMsg: function (content, title, confirm_button, icon, focus_field) {
            $.alert({
                title: title,
                content: content,
                icon: ( typeof icon != 'undefined' ? icon : 'fa fa-info-circle' ),
                confirmButton: ( typeof confirm_button != 'undefined' ? confirm_button : 'OK' ),
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    setTimeout("func_setting_focus('" + focus_field + "')", 500);
                }
            });
        },

        br2nl: function (str) {
            return str.replace(/<br\s*\/?>/mg, "\n");
        },

        // nl2br: function (str) { // Inserts HTML line breaks before all newlines in a string
        //     return str.replace(/([^>])\n/g, '$1<br/>');
        // },

        nl2br: function (str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        },

        replaceAll: function (str, search, replacement) {
            return str.replace(new RegExp(search, 'g'), replacement);
        },

    }, // methods: {

}

function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}
